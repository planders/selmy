# coding=utf-8

import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.md')) as f:
    README = f.read()
with open(os.path.join(here, 'CHANGES.txt')) as f:
    CHANGES = f.read()

requires = [
    'pyramid==1.9.1',  # web framework
    'WTForms',
    'pyramid_debugtoolbar',  # web debugging
    'pyramid_multiauth',  # multiple authentication policies
    'pyramid_tm',  # transaction manager
    'pyramid_exclog',  # exception logging
    'SQLAlchemy',  # ORM
    'transaction',  # SQL transactions
    'zope.sqlalchemy',  # SQL transactions
    'waitress',  # web server
    'pyramid_jinja2',  # server side templates
    'cornice',  # REST library
    'Babel',  # internationalization (i18n) and localization support
    'lingua',  # i18n string extractor
    'alembic',  # Database schema migration
    'pytz',
    'tzlocal',  # get the local timezone
    'python-dateutil',  # other date utilities
    'six',  # Python 2/3 compatibility
    'concurrent-log-handler',

    'prompt_toolkit',

    # TODO: not actually using this yet but maybe
    'authomatic',

    # 'WebError',  # for apex auth - may not need?
    'repoze.tm2',  # ditto - may not need?
    # 'apex==0.9.10dev',  # Authentication (local, OAuth and OpenID)
    # 'git+https://github.com/Preston-Landers/velruse.git',
    # 'git+https://github.com/Preston-Landers/apex.git',
    'Paste',

    'sphinx',  # documentation generator
    'WebTest',  # end-to-end / functional tests
    # 'pytest',  # unit tests
    'nose',
]

test_requires = [
    'coverage',  # unit test coverage reports
    'teamcity-messages',  # Test reporting to TeamCity  (can we move it to test_requires?)
]

setup(name='selmy',
      version='0.0.2',
      description='selmy',
      long_description=README + '\n\n' + CHANGES,
      classifiers=[
          "Programming Language :: Python",
          "Framework :: Pyramid",
          "Topic :: Internet :: WWW/HTTP",
          "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
      ],
      author='Preston Landers',
      author_email='planders@gmail.com',
      url='https://bitbucket.org/planders/selmy',
      keywords='web wsgi bfg pylons pyramid',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      test_suite='selmy.tests',
      install_requires=requires,
      tests_require=test_requires,
      entry_points="""\
      [paste.app_factory]
      main = selmy:main
      [console_scripts]
      initialize_selmy_db = selmy.scripts.initializedb:main
      selmy = selmy.scripts.selmy_console:main
      """,
      )
