Selmy Docs
==========

This directory contains documentation generation scripts for Selmy.

This assumes your Python virtual environment directory is set to $VENV.

To generate the docs, run this:

    $ $VENV/bin/activate   (Windows:  %VENV%\Scripts\activate)
    $ cd docs
    $ make html

Now you can view the docs in build/html/index.html.


TODO
====

 - Serve docs from WSGI static directory
