.. Selmy documentation master file, created by
   sphinx-quickstart on Mon May 26 15:35:05 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Selmy Documentation
===================

Selmy is a tool to help define and manage a database of information. Selmy is both a standalone tool and a framework
for developing more specialized tools. Selmy provides a web-based "CRUD" (Create, Read, Update,
Delete) management interface for your business or personal data that can be used immediately with no programming
knowledge. It also provides programming extensions to create customized applications for a variety of business or
personal objectives.

Throughout this documentation we will refer to your data model by various terms depending on the context: business
objects, domain model, or managed types. The data model is up to you and depends on your particular needs and
applications. We will refer to it generically as "business data" or may refer to "business rules" but this is to be
understood as not limited to commercial enterprises. Selmy works with nearly any data-driven application you have in
mind. Business objects and rules are defined by you, the system owner/operator, as opposed to built-in system data
types or other limitations on functionality.

At its core, Selmy provides an hierarchical abstract data model and provides a REST-ful API to access these data objects
in a consistent manner. These data objects can be managed directly CRUD-style or can be combined into "business forms"
that enforce user-defined rules and relationships between data objects.

Selmy is written in Python and Javascript. It runs on most server platforms such as Windows and Linux and can use
most popular database systems for storage such as PostgreSQL and Microsoft SQL Server.

Contents
========

Table of Contents:

.. toctree::
    :maxdepth: 3

    intro
    proposal

Selmy Overview
==============
:doc:`intro`

REST API Services
=================

Selmy provides a complete RESTful API.

.. services::
   :modules: selmy.views.rest


Python Package API
==================

This section documents the Python package.

selmy package
-------------
.. automodule:: selmy
   :members: main bootstrap get_default_config

selmy.i18n
----------
.. automodule:: selmy.i18n
   :members:


Selmy Data Models
-----------------

The data model is at the core of the application.

selmy.models
^^^^^^^^^^^^
.. automodule:: selmy.models
    :members:


selmy.models.user
^^^^^^^^^^^^^^^^^
.. automodule:: selmy.models.user
    :members:


Selmy Data Views
----------------

Views provide controlled access to the raw data model.

selmy.views
^^^^^^^^^^^
.. automodule:: selmy.views
   :members:

selmy.views.hello
^^^^^^^^^^^^^^^^^
.. automodule:: selmy.views.hello
   :members:

selmy.views.my_view
^^^^^^^^^^^^^^^^^^^
.. automodule:: selmy.views.my_view
   :members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

