module.exports = function (config) {
    config.set(
        {

            basePath: '../',

            preprocessors: {
                'selmy/static/js/ng/**/*.js': ['coverage']
            },

            files: [
                // 'selmy/static/bower_components/jquery/dist/jquery.js',
                // 'selmy/static/bower_components/angular/angular.js',
                // 'selmy/static/bower_components/angular-route/angular-route.js',
                // 'selmy/static/bower_components/angular-mocks/angular-mocks.js',
                // 'selmy/static/bower_components/angular-resource/angular-resource.js',
                'selmy/static/js/ng/selmy-ng.js',
                'selmy/static/js/ng/selmy-ng-*.js',
                'jstest/unit/**/*.js'
            ],

            // autoWatch : true,
            autoWatch: false,

            colors: true,

            frameworks: ['jasmine'],

            browsers: ['Chrome'],

            plugins: [
                'karma-chrome-launcher',
                'karma-firefox-launcher',
                'karma-jasmine',
                'karma-junit-reporter',
                'karma-teamcity-reporter',
                'karma-coverage'
            ],

            junitReporter: {
                outputDir: 'local_data/jstest_data/junit',
                outputFile: 'test_out/unit.xml',
                suite: 'unit'
            },

            reporters: [
                'coverage',
                'progress',
                'junit',
                'teamcity'
            ],

            coverageReporter: {
                reporters: [
                    {type: 'html', dir: 'local_data/jstest_data/coverage-js-html/'},
                    {type: 'teamcity'},
                    {type: 'text-summary'}
                ]
            }
        });
};
