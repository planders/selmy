'use strict';

/* jasmine specs for controllers go here */

describe('selmyApp.controllers', function () {
    beforeEach(module('selmyApp'));


    it('should ....', inject(function ($controller) {
        //spec body
        var types_list_ctrl = $controller('ManageTypesListCtrl', { $scope: {} });
        expect(types_list_ctrl).toBeDefined();
    }));

    it('should ....', inject(function ($controller) {
        //spec body
        var types_detail_ctrl = $controller('ManageTypesDetailCtrl', { $scope: {} });
        expect(types_detail_ctrl).toBeDefined();
    }));
});
