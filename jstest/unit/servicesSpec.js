'use strict';

/* jasmine specs for services go here */

describe('selmyApp.services', function () {
    beforeEach(module('selmyApp'));


    describe('ManagedType', function () {
        it('should return a type for a query', inject(function ($resource) {
            var $mtservice = $resource('ManagedType', { $scope: {} });
            var result = $mtservice.query({name: "Project"});
            console.log(result);
            console.log(result.name);
            expect(result).toBeDefined();
            expect(result.name).toEqual('Project');
        }));
    });
});
