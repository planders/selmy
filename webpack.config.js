const path = require('path');
const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");

// assets.js helps us know which files to copy from
// node_modules (which is not checked into SVN/build)
// to selmy/static/npm (which is)
const Assets = require('./assets');

// Some examples I used to figure out this maze:
// see https://gist.github.com/fruser/df531e9c4e6ee0730e6a
// https://github.com/webpack/webpack/issues/597

// See also production vs. dev config examples:
// https://github.com/Binaryify/vue-loader-example/tree/master/build

module.exports = {
    entry: {
        app: "./selmy/static/js/app.js",
        vue_app: "./selmy/static/js/vue_app.js",
        vendor: [
            'jquery',
            'jquery.cookie',
            'bootstrap',
            // 'lodash',
        ],
    },
    output: {
        path: __dirname + "/selmy/static/webpack/",
        filename: "[name].bundle.js",
        chunkFilename: "[id].bundle.js"
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components|npm|webpack)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['env']
                    }
                }
            },
            {
                test: /\.css$/,
                use: {
                    loader: 'css-loader',
                },
            },
            {
                test: /\.vue$/,
                use: {
                    loader: 'vue-loader',
                    options: {
                        loaders: {
                            js: 'babel!eslint',
                            scss: 'style!css!sass!postcss'
                        },
                        postcss: [
                            require('postcss-cssnext')(),
                            require('postcss-nested')(),
                            require('postcss-mixins')()]
                    }
                }
            }
        ],
        rules: [
            {
                // TODO: limit to vendor stuff?
                test: /\.css$/,
                use: ExtractTextPlugin.extract(
                    {
                        fallback: "style-loader",
                        use: "css-loader"
                    })
            },
            {
                test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: 'url-loader?limit=10000&mimetype=application/font-woff'
            },
            {
                test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: 'file-loader'
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader',
            },
        ],
    },
    resolve: {
        alias: {
            bootstrap: path.resolve(__dirname, `./selmy/static/npm/bootstrap/js/bootstrap.js`),
            vue: path.resolve(__dirname, `./selmy/static/npm/vue/vue.esm.js`),
            'bootstrap.css': path.resolve(
                __dirname, `./selmy/static/npm/bootstrap/css/bootstrap.css`),
            'selmy.css': path.resolve(__dirname, `./selmy/static/css/selmy.css`)
        }
    },
    plugins: [
        new CopyWebpackPlugin(
            Assets.map(asset => {
                // TODO: handle non-list here with default dir
                let asset_path = asset[0];
                let asset_dir = asset[1];
                return {
                    from: path.resolve(__dirname, `./node_modules/${asset_path}`),
                    to: path.resolve(__dirname, `./selmy/static/npm/${asset_dir}`)
                };
            })
        ),
        new webpack.ProvidePlugin(
            {
                // "_": "lodash",
                $: "jquery",
                jQuery: "jquery",
                "window.jQuery": "jquery"
            }),
        new webpack.optimize.CommonsChunkPlugin(
            {name: "vendor", filename: "vendor.js"}
        ),
        new ExtractTextPlugin("vendor.styles.css"),

        // or run with --optimize-minimize
        // new webpack.optimize.UglifyJsPlugin(
        //     {
        //         compress: {
        //             warnings: false
        //         }
        //     }),

        // Stop vue's warnings about dev mode
        // new webpack.DefinePlugin(
        //     {
        //         'process.env': {
        //             NODE_ENV: '"production"'
        //         }
        //     })
    ],
};
