# !/usr/bin/env python
# -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-
# vim: fileencoding=utf-8 tabstop=4 expandtab shiftwidth=4

"""
This ties in the apex/velruse authentication with our notion of a User Profile, which
stores basic facts about the user.

TODO: update this file with new auth system? Authomatic?
"""

from logging import getLogger

from apex.forms import RegisterForm
from apex.lib.libapex import apex_id_from_token
from apex.models import DBSession as ApexDBSession

from selmy.models.user import User, EmailAddress
from selmy.util import u

__author__ = 'Preston Landers (planders@gmail.com)'
__copyright__ = 'Copyright (c) 2017 Preston Landers'
__license__ = 'BSD'

log = getLogger(__name__)


class OpenIdAfter(object):
    @staticmethod
    def after_signup(request, user):
        """
        Initializes the Selmy user profile ("users" table) from a newly created
        OpenID based authentication profile.  Extracts the name and email address from
        the OpenID profile if possible.

        :param request: the request
        :param user: the newly created apex AuthUser object.
        :type user: AuthUser
        :return: None
        """
        apex_id = apex_id_from_token(request)
        apex_profile = apex_id['profile']
        ApexDBSession.flush()  # Needed to get the auth_user.id

        pref_username = u(apex_profile.get('preferredUsername', '<UNKNOWN>'))
        full_name = u(apex_profile.get('displayName', pref_username))
        verified_email = u(apex_profile.get('verifiedEmail', None))
        # other_emails = apex_profile.get('emails', [])

        user.display_name = pref_username
        user_profile = User(name=pref_username, auth_id=user.id, enabled=True, fullname=full_name)
        ApexDBSession.add(user_profile)

        log.info("Adding new OpenID (%s) user! %s : %s", user.provider, user_profile, user)

        ApexDBSession.flush()  # needed to get user_profile.id below

        if verified_email:
            # Assuming OpenID emails are already verified - may be bad assumption
            email = EmailAddress(user_id=user_profile.id, email=verified_email, verified=True)
            ApexDBSession.add(email)
            log.info("Added email address %s", email)


class NewRegisterForm(RegisterForm):
    def after_signup(self, user=None):
        """
        Initialize a Selmy user profile after local provider user registration.
        :param user: The apex AuthUser
        :type user: AuthUser
        :return: None
        """
        user_profile = User(name=user.username, auth_id=user.id, enabled=True,
                            fullname=user.username)
        ApexDBSession.add(user_profile)

        log.info("Registered new user! %s : %s", user_profile, user)

        if user.email:
            ApexDBSession.flush()  # needed to get user_profile.id
            email = EmailAddress(user_id=user_profile.id, email=user.email, verified=False)
            ApexDBSession.add(email)
            log.info("Added email address %s", email)
