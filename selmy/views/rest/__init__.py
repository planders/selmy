# !/usr/bin/env python
# -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-
# vim: fileencoding=utf-8 tabstop=4 expandtab shiftwidth=4

"""
REST object control
"""

import traceback
from logging import getLogger

from pyramid.view import view_config, notfound_view_config

# noinspection PyProtectedMember
from selmy.i18n import _
from selmy.util import highlight_js_to_html, dumps
from selmy.constants import API

__author__ = 'Preston Landers (planders@gmail.com)'
__copyright__ = 'Copyright (c) 2017 Preston Landers'
__license__ = 'BSD'

log = getLogger(__name__)

#: standard keys of return values of many API methods
success = 'success'
errors = 'errors'  # key for standard error messages
messages = 'messages'  # key for standard status messages

#: standard name for collection key
contents = "contents"

JSON_MIME = 'application/json'
accept_json_or_nill = (JSON_MIME, '')


class SelmyRest(object):
    """
    Base class for Selmy REST interface implementations.
    """

    def __init__(self, request):
        self.request = request
        self.translate = request.localizer.translate


def _not_found_json(request):
    """
    Generate the 404 Not Found response dict for JSON
    also used by plain text version.
    :param request: Pyramid request
    :return: response dict with success flag, errors list, and request path
    """
    return {
        success: False,
        errors: [
            request.localizer.translate(
                _("404 Not Found: ${path}"),
                mapping={"path": request.path})
        ],
        "path": request.path,
    }


@notfound_view_config(content_type='json', renderer='json', path_info="/" + API)
def not_found_json(request):
    """
    Handle 404 Not Found for JSON requests.
    """
    request.response.status_code = 404
    return _not_found_json(request)


@notfound_view_config(accept='text/html', renderer='selmy:templates/404.jinja2',
                      path_info="/" + API)
def not_found_html(request):
    """
    Handle 404 Not Found for HTML requests within the API
    """
    request.response.status_code = 404
    return _not_found_json(request)


@notfound_view_config(accept='text/plain', renderer='string', path_info="/" + API)
def not_found_text(request):
    """
    Handle 404 Not Found for plain text requests.

    Needs unit test coverage.
    """
    request.response.status_code = 404
    not_found_data = _not_found_json(request)
    not_found_data['success_str'] = success
    not_found_data['errors_str'] = errors
    not_found_data['errors'] = [str(x) for x in not_found_data['errors']]
    return """\
%(success_str)s: %(success)s
%(errors_str)s: %(errors)s
path: %(path)s
""" % not_found_data


def _api_exception_json(exc, request):
    error_list = [
        {
            "name": exc.__class__.__name__,
            "value": str(exc),
            # TODO: might want to make this as an array of lines instead of one big string
            "traceback": traceback.format_exc()
        },
    ]
    log.warning("Exception handling API request: %s", error_list)
    return {
        success: False,
        errors: error_list,
        "path": request.path,
    }


# TODO: should only handle /api paths! but it an error rendering the browse template seems
# to be able to trigger this. - UPDATE: check if this still happens
# XXX TODO: this has to do with Content-Type versus accept.
# Need to make a content type view config
# https://stackoverflow.com/a/19785439/858289
# https://stackoverflow.com/a/19793395/858289
@view_config(
    content_type='text/html', accept='text/html',
    renderer='selmy:templates/500.jinja2',
    path_info="/" + API, context=Exception)
def api_exception_html(exc, request):
    """
    Generic HTML view for 500 Internal Server Error
    :param exc: exception object
    :param request: Pyramid request
    :return: 500 response HTML with formatted error
    """
    request.response.status_code = 500
    return {
        "error_json": _api_exception_json(exc, request),
        "highlight_js_to_html": highlight_js_to_html,
        "dumps": dumps,
    }


@view_config(
    content_type=JSON_MIME, accept=JSON_MIME,
    renderer='json', path_info="/" + API, context=Exception)
@view_config(
    content_type='', accept=JSON_MIME,
    renderer='json', path_info="/" + API, context=Exception)
def api_exception_json(exc, request):
    request.response.status_code = 500
    return _api_exception_json(exc, request)


@view_config(
    content_type='text/plain', accept='text/plain',
    renderer='string', path_info="/" + API, context=Exception)
def api_exception_text(exc, request):
    """
    Handle an internal error in the API.

    Needs unit test coverage.
    """
    request.response.status_code = 500
    exception_data = _api_exception_json(exc, request)
    exception_data['success_str'] = success
    exception_data['errors_str'] = errors
    # exception_data['errors'] = [str(x) for x in exception_data['errors']]
    error_obj: dict = exception_data['errors'][0]
    exception_data['error_name'] = error_obj['name']
    exception_data['error_msg'] = error_obj['value']
    exception_data['error_tb'] = error_obj['traceback']
    return """\
%(success_str)s: %(success)s
error: %(error_msg)s
error type: %(error_name)s
path: %(path)s
traceback:
%(error_tb)s
""" % exception_data
