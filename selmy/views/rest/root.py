#!/usr/bin/env python
# -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-
# vim: fileencoding=utf-8 tabstop=4 expandtab shiftwidth=4


"""
The API root resource.
"""

from logging import getLogger

from cornice.resource import resource, view

from selmy.exceptions import RequiredValueError
# noinspection PyProtectedMember
from selmy.i18n import _
from selmy.models import get_class_for_table_name, DBSession
from selmy.models.virtual_node import VirtualNode
from selmy.util import u
from selmy.views.rest import SelmyRest, API, success, messages, accept_json_or_nill

__author__ = 'Preston Landers (planders@gmail.com)'
__copyright__ = 'Copyright (c) 2017 Preston Landers'
__license__ = 'BSD'

log = getLogger(__name__)


def add_body_error(request, message):
    # Better way to handle this?
    translated_msg = request.localizer.translate(message)
    if hasattr(request, "errors"):
        request.errors.add('body', 'message', translated_msg)


# noinspection PyUnusedLocal
def rest_validators(request, **kwargs):
    if not request.authenticated_userid:
        add_body_error(request, _('You must be logged in to use the API.'))
    return


@resource(
    collection_path=API,
    validators=rest_validators,
    # permission='authenticated',
    path=API + "/*traverse")
class RestRoot(SelmyRest):
    """
    Currently this handles all REST API requests
    under the /api URL.

    TODO: provide depth parameter?
    limit collection sizes...
    """

    @view(renderer='json')
    def collection_get(self):
        res = self.request.context
        # return {contents: list(res)}
        log.info("Getting Root collection: %s", res)
        return res

    # NOTE: default view should be LAST
    @view(accept='text/plain', renderer='string')
    @view(accept=accept_json_or_nill, renderer='json')
    def get(self):
        """
        GET a resource in JSON format.

        :return: resource object rendered as JSON
        """
        res = self.request.context
        if res is None:
            add_body_error(self.request, _('Invalid object requested.'))
            self.request.response.status_code = 500  # TODO: right code?
            return {}
        log.info("GET by name %s: %s", res.name, res)
        # TODO: change string renderer
        return res

    @view(accept='text/plain', renderer='string')
    @view(accept=accept_json_or_nill, renderer='json')
    def delete(self):
        """
        DELETE a resource.

        :return: Result description (not original object) rendered as JSON or plain text
        """
        res = self.request.context
        log.info("DELETE item name %s: %s", res.name, res)
        result = {
            success: True,
            'path': self.request.path,
        }
        self._delete_resource(res)
        result[messages] = [
            self.translate(_('Resource was deleted.')),
        ]

        # TODO: handle text response...!
        return result

    @view(accept=accept_json_or_nill, renderer='json')
    def patch(self):
        """
        PATCH (update) a resource from a JSON representation.

        :return: updated resource object rendered as JSON
        """
        context = self.request.context
        patch_json = self.request.json_body
        result = self._update_resource_from_json(context, patch_json)

        log.info("PATCH item name %s: %s ==> %s", context.name, context, patch_json)
        return result

    @view(accept=accept_json_or_nill, renderer='json')
    def put(self):
        """
        PUT (create or update) a resource from a JSON representation.

        :return: new or updated resource object rendered as JSON
        """
        context = self.request.context
        put_json = self.request.json_body
        result = self._update_or_create_resource_from_json(context, put_json, allow_update=True)

        log.info("PUT item name %s: %s ==> %s", context.name, context, put_json)
        return result

    @view(accept=accept_json_or_nill, renderer='json')
    def post(self):
        """
        POST (create only) a resource from a JSON representation.

        :return: new resource object rendered as JSON
        """
        context = self.request.context
        post_json = self.request.json_body
        result = self._update_or_create_resource_from_json(context, post_json, allow_update=False)

        log.info("POST item name %s: %s ==> %s", context.name, context, post_json)
        return result

    @staticmethod
    def _update_resource_from_json(res, patch_json):
        # TODO: Should we allow parent change?
        res.update_from_json(patch_json)
        return res

    def _update_or_create_resource_from_json(self, res, put_json, allow_update=True):
        """
        Create or updated (as needed) as resource using a JSON representation.

        :param res: Parent resource to update
        :type res: selmy.models.NodeMixin
        :param put_json: decoded JSON object
        :type put_json: dict
        :param allow_update: Allow updating an existing object (if False, only create is allowed)
        :type allow_update: bool
        :return:
        """
        try:
            put_name = u(put_json['name'])
        except KeyError:
            raise RequiredValueError(
                self.translate(_("Object must contain the 'name' attribute.")))
        try:
            existing_item = res[put_name]
        except KeyError:
            existing_item = None

        if existing_item is not None:
            if not allow_update:
                val_error_msg = self.translate(
                    _("You may not POST an object that already exists. "
                      "Consider using PUT instead."))
                raise ValueError(val_error_msg)
            log.info("Update existing resource: %s: %s ==> %s", existing_item.name, existing_item,
                     put_json)
            return self._update_resource_from_json(existing_item, put_json)

        log.info("Create new resource %s under: %s", put_json, res)

        try:
            resource_type = u(put_json['resource_type'])
        except KeyError:
            raise RequiredValueError(
                self.translate(_("Object must contain a valid 'resource_type' attribute.")))

        resource_class = get_class_for_table_name(resource_type)
        if not resource_class:
            raise ValueError(
                self.translate(
                    _("Unable to find class associated with resource_type: ${resource_type}"),
                    mapping={
                        "resource_type": resource_type
                    }
                )
            )

        # noinspection PyUnresolvedReferences
        new_item = resource_class.create(name=put_name)
        # new_item.name = put_name

        self.set_parent(res, new_item)

        DBSession.add(new_item)
        DBSession.flush()

        # not sure this is actually necessary anymore
        with DBSession.no_autoflush:
            new_item.ensure_collections()
            new_item.update_from_json(put_json)

            # Force the parent again
            self.set_parent(res, new_item)

        DBSession.flush()
        log.info("Created new item: %s", new_item)
        return new_item

    @staticmethod
    def set_parent(parent_res, child_res):
        # If 'res' is a VirtualNode, the real parent_id should be null
        child_res._resource_parent = None
        if isinstance(parent_res, VirtualNode):
            child_res._resource_parent = parent_res
            child_res.parent_id = None
        else:
            child_res.parent = parent_res
            child_res.parent_id = parent_res.id

    @staticmethod
    def _delete_resource(res):
        log.warning("Deleting resource! %s: %s", res.name, res)
        DBSession.delete(res)
