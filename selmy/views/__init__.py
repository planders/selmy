# coding=utf-8

from pyramid.view import notfound_view_config

__author__ = 'Preston Landers (planders@gmail.com)'


@notfound_view_config(renderer='selmy:templates/404.jinja2')
def not_found_html(request):
    """
    Generic HTML view for 404 Not Found
    :param request: Pyramid request
    :return:
    """
    request.response.status_code = 404
    return {}


# Need a 403 Forbidden handler.
