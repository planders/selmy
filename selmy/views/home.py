# !/usr/bin/env python
# -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-
# vim: fileencoding=utf-8 tabstop=4 expandtab shiftwidth=4

#
# Copyright (c) 2017 Preston Landers
# Licensed Materials - All Rights Reserved
#

"""
Selmy data views
"""

from logging import getLogger

from pyramid.response import Response
from pyramid.view import view_config
from sqlalchemy.exc import DBAPIError

__author__ = 'Preston Landers (planders@gmail.com)'
__copyright__ = 'Copyright (c) 2017 Preston Landers'
__license__ = 'BSD'

log = getLogger(__name__)


@view_config(route_name='home', renderer='selmy:templates/home.jinja2')
def home_view(request):
    try:
        # request.session.flash("Virtual Insanity.")
        one = "TEMPORARY"
        log.warning("Hello, got result: %s", one)

    except DBAPIError:
        return Response(conn_err_msg, content_type='text/plain', status_int=500)
    return {
        'root': request.context,
        'project': request.product_config.product_name,
        'copyright': 'Copyright (c) 2017 Preston Landers'}


conn_err_msg = """\
The application is having a problem using your SQL database.  The problem
might be caused by one of the following things:

1.  You may need to run the "initialize_selmy_db" script
    to initialize your database tables.  Check your virtual 
    environment's "bin" directory for this script and try to run it.

2.  Your database server may not be running.  Check that the
    database server referred to by the "sqlalchemy.url" setting in
    your "development.ini" file is running.

After you fix the problem, please restart the Pyramid application to
try it again.
"""
