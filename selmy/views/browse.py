#!/usr/bin/env python
# -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-
# vim: fileencoding=utf-8 tabstop=4 expandtab shiftwidth=4

"""
Browse resources.
"""

from logging import getLogger
from pprint import pformat

from pyramid.location import lineage
from pyramid.view import view_config

from selmy.util import highlight_js_to_html, dumps

__author__ = 'Preston Landers (planders@gmail.com)'
__copyright__ = 'Copyright (c) 2017 Preston Landers'
__license__ = 'BSD'

log = getLogger(__name__)


def walk_path(resource):
    return reversed(list(lineage(resource)))


# noinspection PyUnusedLocal
@view_config(route_name='browse_noslash',
             renderer='selmy:templates/browse.jinja2', permission='authenticated')
@view_config(route_name='browse',
             renderer='selmy:templates/browse.jinja2', permission='authenticated')
def browse_view(request):
    return dict(
        walk_path=walk_path,
        dumps=dumps,
        pformat=pformat,
        highlight_js_to_html=highlight_js_to_html,
    )
