# !/usr/bin/env python
# -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-
# vim: fileencoding=utf-8 tabstop=4 expandtab shiftwidth=4

"""
Data Object Management view
"""

from logging import getLogger

from pyramid.view import view_config

# from selmy.i18n import _


__author__ = 'Preston Landers (planders@gmail.com)'
__copyright__ = 'Copyright (c) 2017 Preston Landers'
__license__ = 'BSD'

log = getLogger(__name__)


@view_config(route_name='manage', renderer='selmy:templates/manage.jinja2')
def manage_view(request):
    return {}
