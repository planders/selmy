# !/usr/bin/env python
# -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-
# vim: fileencoding=utf-8 tabstop=4 expandtab shiftwidth=4

"""
The Hello view - just an example showing translatable strings.
"""

from logging import getLogger

from pyramid.view import view_config

from selmy.i18n import _

__author__ = 'Preston Landers (planders@gmail.com)'
__copyright__ = 'Copyright (c) 2017 Preston Landers'
__license__ = 'BSD'

log = getLogger(__name__)


@view_config(route_name='test', renderer='selmy:templates/hello.jinja2', permission='authenticated')
def junk_view(request):
    return {
        'title': 'testing',
        'project': 'Selmy',
        'copyright': 'Copyright (c) 2017 Preston Landers',
        'x': "asdf",
        'secret': '12345',
    }


@view_config(route_name='hello', renderer='selmy:templates/hello.jinja2')
def hello_view(request):
    # NOTE: This is a demonstration of a translatable string template showing parameter mapping
    x = _("x1", default="Hello, world. Secret: ${secret}", mapping={'secret': 42})

    # This version is translated into the current user's actual language.
    x_t = request.localizer.translate(x, mapping={"secret": "99"})

    log.warning("In hello_view: x: %(x)s\nRequest: %(request)s\n",
                {'request': request, 'x': x_t})

    return {
        # 'title': 'Hello, world',
        'project': 'Selmy',
        'copyright': 'Copyright (c) 2017 Preston Landers',
        'x': x_t,
        'secret': 'qqqq',
    }
