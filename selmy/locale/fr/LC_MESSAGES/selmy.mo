��    F      L              |  I   }     �     �     �     �       	        !     5     >     D     d     j     r     y  1   �     �     �     �     �     �     �     �          
  1        C     L     Q     g     p     w     �     �     �     �  5   �     �     �     �                    "     +  C   >  	   �     �     �     �  !   �  ,   �       <   4     q  (   w  &   �  /   �     �  
   		     	     &	     -	     >	     N	     f	     	     �	     �	  w  �	  P        W     c      j     �  	   �     �     �     �     �  *   �                      4   !     V  
   b     m     ~     �     �     �     �     �  6   �  	   �                    &  	   .  
   8     C     I     d  C   y     �  	   �     �     �     �     �            P         q  '   ~  !   �     �      �  2   �     !  3   >     r  8   y  -   �  1   �          1     B     T     Y     l     {     �     �     �  -   �       Available commands:
        * quit
        * help
        * show
     About This Site Amelia Apex and Velruse Authentication Bootswatch Themes Browse Browsing: By: Preston Landers Cerulean Class Command to run (default: shell) Cosmo Created Cyborg Darkly Department of Computer and Electrical Engineering Description Flatly Great, thanks! Hello! Is Powered By: JetBrains PyCharm Journal Lumen Manage Managed (administrator defined) type definitions. Modified Name Pyramid Web Framework Readable Report Resource Search Selmy Selmy Application Platform Selmy Home Page Selmy application server console for administrators.
 Selmy>  Sign In Sign Out Signed in as Simplex Slate Spacelab Special thanks to: Specify the config_uri for the app. Overrides SELMY_CONFIG env var. Superhero System objects and collections. System type definitions. Test The University of Texas at Austin The intuitive business application platform. The secret is: %(secret)s! The show command reports information about the named object. Theme This item has no other items beneath it. This site is under active development. This site was created with the following tools: Toggle navigation Tools Used Twitter Bootstrap United Unknown command. Unknown object. Welcome to %(product)s. Welcome to Selmy ${ver}! Yeti jQuery x1 Project-Id-Version: selmy 0.0
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2014-06-15 12:56-0500
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: fr <LL@li.org>
Plural-Forms: nplurals=2; plural=(n > 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 1.3
     Les commandes disponibles:
        * quit
        * help
        * show
     Sur ce Site Amelia Apex et authentification Velruse Bootswatch thèmes Parcourir Navigation : Par : Preston Landers Cerulean Classe Commande à exécuter (par défaut: shell) Cosmo Créé Cyborg Sombre Département d'informatique et de génie électrique Description Carrément Génial, Merci ! Bonjour! Est alimenté par : JetBrains PyCharm Journal Lumen Gérer Définitions de type managé (administrateur défini). Modifiée Nom Pyramide Web Framework Lisible Rapport Ressource Rechercher Selmy Selmy Application Platform Page d'accueil Selmy Console du serveur d'applications de Selmy pour les administrateurs Selmy >  Connexion Se déconnecter Signé en tant que Simplex Ardoise Spacelab Spécial Merci à : Spécifiez le config_uri pour l'application. Remplacements SELMY_CONFIG env var. Super-héros Les collections et les objets système. Définitions de type de système. Test L'Université du Texas à Austin La plateforme d'applications entreprise intuitive. %(secret)s, c'est le secret! La commande show vous renseigne sur l'objet nommé. Thème Cet article n'a aucun autre élément situé en dessous. Ce site est en cours de développement actif. Ce site a été créé avec les outils suivants : Activer/désactiver navigation Outils utilisés Twitter Bootstrap Unie Commande inconnue. Objet inconnu. Bienvenue sur %(product)s. Bienvenue à Selmy ${ver}! Yeti jQuery Bonjour, le monde. Le secret c'est: ${secret} 