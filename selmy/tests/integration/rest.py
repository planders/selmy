#!/usr/bin/env python
# -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-
# vim: fileencoding=utf-8 tabstop=4 expandtab shiftwidth=4

"""
Functional test of the REST HTTP interface
"""

from __future__ import print_function

from logging import getLogger

from selmy.tests.integration import FunctionalTests
from selmy.util import u

__author__ = 'Preston Landers (planders@gmail.com)'
__copyright__ = 'Copyright (c) 2017 Preston Landers'
__license__ = 'BSD'

log = getLogger(__name__)

# : All (?) nodes should have these keys...
required_keys = [
    "uri",
    "resource_type",
    "id",
    "name",
]


class RESTErrorViewsTest(FunctionalTests):
    def test_404_view(self):
        res = self.testapp.get('/api/does_not_exist', status=404)
        log.info("404 view result: %s", res)

    def test_500_view(self):
        res = self.testapp.post_json(
            "/api/",
            dict(
                name="System",
                resource_type="SystemRoot"
            ),
            status=500)
        log.info("500 view result: %s", res)

    def test_put_missing_name(self):
        res = self.testapp.put_json(
            "/api/System",
            {},
            status=500)
        log.info("500 view result: %s", res)
        json_body = res.json_body
        self.assertFalse(json_body['success'])

        # This is brittle
        self.assertEqual(
            u("Object must contain the 'name' attribute."),
            json_body['errors'][0]['value']
        )

    def test_put_missing_resource_type(self):
        res = self.testapp.put_json(
            "/api/System",
            {
                "name": "Thingie",
            },
            status=500)
        log.info("500 view result: %s", res)
        json_body = res.json_body
        self.assertFalse(json_body['success'])

        # This is brittle
        self.assertEqual(
            u("Object must contain a valid 'resource_type' attribute."),
            json_body['errors'][0]['value']
        )

    def test_put_invalid_resource_type(self):
        res = self.testapp.put_json(
            "/api/System",
            {
                "name": "Thingie",
                "resource_type": "Bogus!",
            },
            status=500)
        log.info("500 view result: %s", res)
        json_body = res.json_body
        self.assertFalse(json_body['success'])

        # This is brittle
        self.assertEqual(
            u('Unable to find class associated with resource_type: Bogus!'),
            json_body['errors'][0]['value']
        )


class RESTBasicStructureTest(FunctionalTests):
    def test_home(self):
        res = self.testapp.get('/api', status=200)
        self.assertTrue('uri' in res.text)

        for req_key in required_keys:
            self.assertTrue(req_key in res.json_body)

        self.assertEqual(res.json_body['uri'], 'http://localhost/api')


class RESTTraverse(FunctionalTests):
    def __init__(self, methodName='runTest'):
        super(RESTTraverse, self).__init__(methodName)
        self._visited = set()

    def test_system_urls(self):
        # self.skipTest("Takes too long.")
        self.recursive_get("/api/System")

    def recursive_get(self, path):
        res = self.testapp.get(path, status=200)
        self.assertTrue('uri' in res.text)
        self._visited.add(path)

        for req_key in required_keys:
            self.assertTrue(req_key in res.json_body)

        my_uri = res.json_body["uri"]

        if "contents" in res.json_body:
            for child_object in res.json_body["contents"]:
                if "uri" in child_object:
                    child_uri = child_object["uri"]
                    log.info("Recursing from %s => %s", my_uri, child_uri)
                    print("Recursing from %s => %s" % (my_uri, child_uri))
                    if child_uri not in self._visited:
                        self.recursive_get(child_uri)
                    else:
                        log.warning("Tried to visit URI again: %s", child_uri)
