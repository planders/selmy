# !/usr/bin/env python
# -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-
# vim: fileencoding=utf-8 tabstop=4 expandtab shiftwidth=4

"""
Integration and functional end-to-end tests using WebTest library.
"""

import os
import re
from logging import getLogger

from paste.deploy import loadapp

from selmy.tests import SelmyTestCase
from selmy.util import u

__author__ = 'Preston Landers (planders@gmail.com)'
__copyright__ = 'Copyright (c) 2017 Preston Landers'
__license__ = 'BSD'

log = getLogger(__name__)


def project_root_dir():
    # TODO: assuming running script from root dir
    return os.getcwd()


class FunctionalTests(SelmyTestCase):
    """
    Base class for all functional tests using WebTest.
    """
    # TODO: automatically create these accounts if needed
    user_login = "test_user"
    user_password = "asdf"

    use_auth = True

    @classmethod
    def setUpClass(cls):
        super(FunctionalTests, cls).setUpClass()
        from selmy import get_default_config
        cls.app = loadapp("config:" + get_default_config(), relative_to=project_root_dir())

    def setUp(self):
        from webtest import TestApp
        # app = main(get_default_config())
        self.testapp = TestApp(self.app, extra_environ=dict(REMOTE_ADDR='127.0.0.1'))
        super(FunctionalTests, self).setUp()
        if self.use_auth:
            self.setup_authentication()

    def setup_authentication(self):
        user_login = self.user_login
        user_password = self.user_password
        login_page = self.testapp.get("/auth/login")
        token_RE = re.compile(r"[?]csrf_token=(\w+?)[&]")
        csrf_token = token_RE.search(login_page.text).group(1)
        # request.session.get_csrf_token()
        res = self.testapp.post("/auth/login", params={
            "login": user_login,
            "password": user_password,
            "csrf_token": csrf_token,
        })
        if res.text.find("Login Error") != -1:
            raise RuntimeError("Unable to log in with user account: %s" % (user_login,))
        log.info("Logged in as user: %s => %s", user_login, res)

    def tearDown(self):
        if self.use_auth:
            self.testapp.get("/auth/logout")
        super(FunctionalTests, self).tearDown()


class BasicStructureTest(FunctionalTests):
    """
    Test a few basic URLs.
    """
    def test_home(self):
        res = self.testapp.get('/', status=200)
        self.assertTrue('Pyramid' in res.text)

    def test_failing_html_view(self):
        res = self.testapp.get('/doesnotexist', status=404)
        self.assertEqual(res.status_int, 404)

    def test_browse(self):
        res = self.testapp.get("/browse", status=200)
        self.assertTrue('<h4>Root node</h4>' in res.text)
        self.assertTrue('Details' in res.text)


class CheckAuthRequired(FunctionalTests):
    """
    Try accessing auth-protected resources without logging in
    and verify that we got errors.
    """
    use_auth = False

    def test_browse(self):
        res = self.testapp.get("/browse", status=302)
        redir_location = res.headers["Location"]
        # make sure we got redirected to the login screen.
        self.assertNotEqual(redir_location.find("/auth/login"), -1)

    def test_api(self):
        res = self.testapp.get("/api/System", status=400)

        self.assertEqual(res.json_body["status"], "error")

        self.assertEqual(res.json_body["errors"][0]["description"],
                         u("You must be logged in to use the API."))
