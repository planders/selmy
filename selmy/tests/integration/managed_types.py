#!/usr/bin/env python
# -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-
# vim: fileencoding=utf-8 tabstop=4 expandtab shiftwidth=4

"""
Functional test of the REST HTTP interface: Managed Types and Objects.
"""

from __future__ import print_function

from datetime import date, datetime
from decimal import Decimal
from logging import getLogger

from pytz import UTC
from six.moves.urllib_parse import quote

from selmy.models import MANAGED_TYPE_ROOT, SYSTEM, TYPE_ROOT
from selmy.models.managed import ManagedFieldDataType
from selmy.tests.integration import FunctionalTests
from selmy.util import (
    u, utc_now, get_json_renderer, parse_iso_date, parse_iso_datetime, parse_decimal
)
from selmy.views.rest import API

__author__ = 'Preston Landers (planders@gmail.com)'
__copyright__ = 'Copyright (c) 2017 Preston Landers'
__license__ = 'BSD'

log = getLogger(__name__)

_contents = "contents"

json_renderer = get_json_renderer()({})

JSON_MIME = 'application/json'

# TODO: is this reasonable?
# Comparison tolerance for determining if datetimes are equal in some our tests
# I'm not sure why this difference is happening in fields we're not modifying.
# Could be indicative of a bug somewhere...
datetime_second_tolerance = 0.002


class ManagedTypesRESTTest(FunctionalTests):
    """
    Create a new managed type and add some records under it.
    """

    @classmethod
    def setUpClass(cls):
        super(ManagedTypesRESTTest, cls).setUpClass()

        cls.obj_uri_list = []
        cls.sub_obj_uri_list = []

        # Need to get data_type_id of these fields:
        cls.text_type = ManagedFieldDataType.get_type_by_capability(ManagedFieldDataType.UNICODE)
        cls.float_type = ManagedFieldDataType.get_type_by_capability(ManagedFieldDataType.FLOAT)
        cls.integer_type = ManagedFieldDataType.get_type_by_capability(ManagedFieldDataType.INTEGER)
        cls.decimal_type = ManagedFieldDataType.get_type_by_capability(ManagedFieldDataType.DECIMAL)
        cls.bool_type = ManagedFieldDataType.get_type_by_capability(ManagedFieldDataType.BOOLEAN)
        cls.date_type = ManagedFieldDataType.get_type_by_capability(ManagedFieldDataType.DATE)
        cls.datetime_type = ManagedFieldDataType.get_type_by_capability(
            ManagedFieldDataType.DATETIME)

        cls.decimal_type_id = cls.decimal_type.id
        cls.text_type_id = cls.text_type.id
        cls.bool_type_id = cls.bool_type.id
        cls.float_type_id = cls.float_type.id
        cls.integer_type_id = cls.integer_type.id
        cls.date_type_id = cls.date_type.id
        cls.datetime_type_id = cls.datetime_type.id

        cls._managedTypeName = "Test Widget"
        cls._managedFieldName_FluxRate = "Flux Rate"  # Decimal
        cls._managedFieldName_Active = "Active"  # Boolean
        cls._managedFieldName_Note = "Note"  # Text
        cls._managedFieldName_BoopCount = "Boop Count"  # Integer
        cls._managedFieldName_BoopRate = "Boop Rate"  # Float
        cls._managedFieldName_ExpireDate = "Expiration Date"  # Date
        cls._managedFieldName_ActiveDateTime = "Active Datetime"  # Datetime

        cls._all_managed_fields = [
            cls._managedFieldName_FluxRate,
            cls._managedFieldName_Active,
            cls._managedFieldName_Note,
            cls._managedFieldName_BoopCount,
            cls._managedFieldName_BoopRate,
            cls._managedFieldName_ExpireDate,
            cls._managedFieldName_ActiveDateTime,
        ]

        cls._managedSubTypeName = "Invoice"
        cls._managedSubFieldName_BillRate = "Bill Rate"  # Decimal
        cls._managedSubFieldName_Units = "Units"  # Decimal
        cls._managedSubFieldName_UnitNote = "Unit Note"  # Text
        cls._managedSubFieldName_Paid = "Paid"  # Boolean
        cls._managedSubFieldName_BillDate = "Bill Date"  # Date

        cls._all_sub_managed_fields = [
            cls._managedSubFieldName_BillRate,
            cls._managedSubFieldName_Units,
            cls._managedSubFieldName_UnitNote,
            cls._managedSubFieldName_Paid,
            cls._managedSubFieldName_BillDate,
        ]

        cls._managed_field = "managed_field"

        cls._managedTypeJSON = {
            "name": cls._managedTypeName,
            "resource_type": "managed_type",
            _contents: [
                # Normal data fields
                {
                    "name": cls._managedFieldName_Active,
                    "resource_type": cls._managed_field,
                    "order": 1,
                    "data_type_id": cls.bool_type_id,
                },
                {
                    "name": cls._managedFieldName_FluxRate,
                    "resource_type": cls._managed_field,
                    "order": 2,
                    "data_type_id": cls.decimal_type_id,
                },
                {
                    "name": cls._managedFieldName_Note,
                    "resource_type": cls._managed_field,
                    "order": 3,
                    "data_type_id": cls.text_type_id,
                },
                {
                    "name": cls._managedFieldName_BoopCount,
                    "resource_type": cls._managed_field,
                    "order": 4,
                    "data_type_id": cls.integer_type_id,
                },
                {
                    "name": cls._managedFieldName_BoopRate,
                    "resource_type": cls._managed_field,
                    "order": 5,
                    "data_type_id": cls.float_type_id,
                },
                {
                    "name": cls._managedFieldName_ExpireDate,
                    "resource_type": cls._managed_field,
                    "order": 6,
                    "data_type_id": cls.date_type_id,
                },
                {
                    "name": cls._managedFieldName_ActiveDateTime,
                    "resource_type": cls._managed_field,
                    "order": 7,
                    "data_type_id": cls.datetime_type_id,
                },
            ]
        }

        cls._managedSubTypeJSON = {
            # Subtype definition
            "name": cls._managedSubTypeName,
            "resource_type": "managed_type",
            _contents: [
                {
                    "name": cls._managedSubFieldName_BillRate,
                    "resource_type": cls._managed_field,
                    "order": 1,
                    "data_type_id": cls.decimal_type_id,
                },
                {
                    "name": cls._managedSubFieldName_Units,
                    "resource_type": cls._managed_field,
                    "order": 2,
                    "data_type_id": cls.decimal_type_id,
                },
                {
                    "name": cls._managedSubFieldName_UnitNote,
                    "resource_type": cls._managed_field,
                    "order": 3,
                    "data_type_id": cls.text_type_id,
                },
                {
                    "name": cls._managedSubFieldName_Paid,
                    "resource_type": cls._managed_field,
                    "order": 4,
                    "data_type_id": cls.bool_type_id,
                },
                {
                    "name": cls._managedSubFieldName_BillDate,
                    "resource_type": cls._managed_field,
                    "order": 5,
                    "data_type_id": cls.date_type_id,
                },
            ]
        }

        cls._managedSub2TypeName = "Test Run"
        cls._managedSub2FieldName_Passed = "Passed"  # Boolean
        cls._managedSub2FieldName_LastRun = "Last Run"  # Datetime

        # Subtype 2 definition
        cls._managedSubType2JSON = {
            "name": cls._managedSub2TypeName,
            "resource_type": "managed_type",
            _contents: [
                {
                    "name": cls._managedSub2FieldName_LastRun,
                    "resource_type": cls._managed_field,
                    "order": 1,
                    "data_type_id": cls.datetime_type_id,
                },
                {
                    "name": cls._managedSub2FieldName_Passed,
                    "resource_type": cls._managed_field,
                    "order": 2,
                    "data_type_id": cls.bool_type_id,
                },
            ]
        }

        # TODO: make a function
        cls._managed_system_type = '/' + '/'.join([API, SYSTEM, TYPE_ROOT, MANAGED_TYPE_ROOT])
        cls._managedTypeUri = cls._managed_system_type + '/' + quote(cls._managedTypeName)
        cls._managedSubTypeUri = cls._managedTypeUri + '/' + quote(cls._managedSubTypeName)
        cls._managedSub2TypeUri = cls._managedTypeUri + '/' + quote(cls._managedSub2TypeName)

        cls._managedCollectionUri = '/' + API + '/' + quote(cls._managedTypeName)

        # TODO: we need to test pagination of results, so maybe set this higher later
        cls._num_objects_to_create = 4  # how many "Test Widgets"?
        cls._num_sub_objects_to_create = 3  # how many "Invoices" per Test Widget?

    @staticmethod
    def _render_json(info):
        return json_renderer(info, {})

    @staticmethod
    def _get_json_headers():
        return {
            "accept": JSON_MIME,
        }

    def test_01_check_managed_type_does_not_exist(self):
        """Verify that our managed types don't already exist."""
        res = self.testapp.get(
            self._managedTypeUri,
            headers=self._get_json_headers(),
            status=404)
        log.info("Result: %s", res)
        res_sub = self.testapp.get(
            self._managedSubTypeUri,
            headers=self._get_json_headers(),
            status=404)
        log.info("Subtype Result: %s", res_sub)

    def test_02_create_managed_type(self):
        """Create a managed type, some fields, and a nested subfield."""
        res = self.testapp.post_json(
            self._managed_system_type,
            self._managedTypeJSON,
            status=200)
        self.assertTrue('uri' in res.text)
        json_body = res.json_body
        self.assertEqual(
            json_body['uri'],
            'http://localhost' + self._managedTypeUri)
        self.assertEqual(
            json_body['resource_type'],
            'managed_type')

        res_sub = self.testapp.post_json(
            self._managedTypeUri,
            self._managedSubTypeJSON,
            status=200)
        self.assertTrue('uri' in res_sub.text)
        json_body = res_sub.json_body
        self.assertEqual(
            json_body['uri'],
            'http://localhost' + self._managedSubTypeUri)
        self.assertEqual(
            json_body['resource_type'],
            'managed_type')

    def test_03_fetch_managed_types(self):
        """Fetch and validate the managed types that we just created."""
        # Try to fetch the main type again
        res = self.testapp.get(
            self._managedTypeUri,
            headers=self._get_json_headers(),
            status=200)
        self.assertEqual(
            res.json_body['uri'],
            'http://localhost' + self._managedTypeUri)
        log.info("Result: %s", res)
        self._check_field_names(
            res.json_body, self._all_managed_fields + [self._managedSubTypeName])

        found_subtype = None
        for sub_item in res.json_body[_contents]:
            if sub_item['name'] == self._managedSubTypeName:
                found_subtype = sub_item
        self.assertIsNotNone(found_subtype)

        res_sub = self.testapp.get(
            self._managedSubTypeUri,
            headers=self._get_json_headers(),
            status=200)
        log.info("Result: %s", res_sub)
        self.assertEqual(
            res_sub.json_body['uri'],
            'http://localhost' + self._managedSubTypeUri)
        self._check_field_names(res_sub.json_body, self._all_sub_managed_fields)

    def _check_field_names(self, resource, field_list):
        """Check that we got the correct field names back."""
        # Check that all retrieved names are valid.
        for item in resource[_contents]:
            self.assertIn(item['name'], field_list)
        resource_field_names = [item['name'] for item in resource[_contents]]
        # Check that all expected names are present.
        for field_name in field_list:
            self.assertIn(field_name, resource_field_names)

    def test_10_post_managed_objects(self):
        """
        Create several Managed Objects (Sample Widgets) plus some instance of the sub-type
        (Invoice) for each one.
        """
        for object_i in range(1, self._num_objects_to_create + 1):
            object_name = "Sample Widget %s" % (object_i,)
            expected_uri = 'http://localhost' \
                           + self._managedCollectionUri + '/' + quote(object_name)
            self.obj_uri_list.append(expected_uri)

            expected_flux_rate = Decimal("242.2002")
            expected_active = True
            expected_note = u("Note for test item %s" % (object_i,))
            expected_boop_count = 42 + object_i
            expected_boop_rate = 19.65 * object_i
            expected_expire_date = date(2024, 6, (object_i % 31) + 1)
            expected_active_datetime = utc_now()
            expected_active_datetime.replace(2014, 6, (object_i % 31) + 1)
            expected_results = {
                self._managedFieldName_FluxRate: expected_flux_rate,
                self._managedFieldName_Active: expected_active,
                self._managedFieldName_Note: expected_note,
                self._managedFieldName_BoopCount: expected_boop_count,
                self._managedFieldName_BoopRate: expected_boop_rate,
                self._managedFieldName_ExpireDate: expected_expire_date,
                self._managedFieldName_ActiveDateTime: expected_active_datetime,
            }

            obj_dict = dict(
                name=object_name,
                resource_type="managed_object",
                contents=self.expand_values_dict(expected_results),
            )

            obj_encoded = self._render_json(obj_dict)

            res = self.testapp.post(
                self._managedCollectionUri,
                obj_encoded,
                content_type=JSON_MIME,
                status=200)
            json_body = res.json_body
            self.assertEqual(
                json_body['uri'],
                expected_uri)
            self.assertEqual(
                json_body['resource_type'],
                'managed_object')
            self.validate_content_values(json_body, expected_results)

            for sub_i in range(1, self._num_sub_objects_to_create + 1):
                sub_name = "%s %s for Widget %s" % (self._managedSubTypeName, sub_i, object_i,)
                sub_post_url = 'http://localhost' \
                               + self._managedCollectionUri + '/' + quote(object_name) + \
                               '/' + quote(self._managedSubTypeName)
                sub_uri = sub_post_url + '/' + quote(sub_name)

                self.sub_obj_uri_list.append(sub_uri)

                sub_expected_bill_rate = Decimal('192.1393') * sub_i
                sub_expected_units = 24 + sub_i
                sub_expected_unit_note = u("Note for test item: %s" % (sub_uri,))
                sub_expected_paid = False
                sub_expected_bill_date = date(2011, 6, 24)

                sub_expected_results = {
                    self._managedSubFieldName_BillRate: sub_expected_bill_rate,
                    self._managedSubFieldName_Units: sub_expected_units,
                    self._managedSubFieldName_UnitNote: sub_expected_unit_note,
                    self._managedSubFieldName_Paid: sub_expected_paid,
                    self._managedSubFieldName_BillDate: sub_expected_bill_date,
                }

                sub_dict = dict(
                    name=sub_name,
                    resource_type="managed_object",
                    contents=self.expand_values_dict(sub_expected_results),
                )

                sub_encoded = self._render_json(sub_dict)

                sub_res = self.testapp.post(
                    sub_post_url,
                    sub_encoded,
                    content_type=JSON_MIME,
                    status=200)
                sub_body = sub_res.json_body
                self.assertEqual(
                    sub_body['uri'],
                    sub_uri)
                self.assertEqual(
                    sub_body['resource_type'],
                    'managed_object')
                self.validate_content_values(sub_body, sub_expected_results)

        return

    @staticmethod
    def expand_values_dict(values_dict):
        ret = []
        for key, value in values_dict.items():
            ret.append(dict(
                name=key,
                value=value,
            ))
        return ret

    def validate_content_values(self, res, expected_results):
        for child in res[_contents]:
            if child['name'] in expected_results:
                actual_val = child['value']
                expected_val = expected_results[child['name']]

                capability = child.get('capability', '')
                actual_val = self.coerce_values(actual_val, expected_val, capability)

                if isinstance(actual_val, datetime):
                    time_diff_sec = abs((actual_val - expected_val).total_seconds())
                    if time_diff_sec > datetime_second_tolerance:
                        self.fail(
                            "Datetimes differ more than %s seconds (%s diff): actual %s -- "
                            "expected %s" % (
                                datetime_second_tolerance, time_diff_sec, actual_val, expected_val))
                else:
                    self.assertEqual(expected_val, actual_val)
        return

    @staticmethod
    def coerce_values(actual_val, expected_val=None, capability=None):
        # TODO: don't we have a good way to do this in this direction?
        # A function that combines this block?
        if capability == 'datetime' or isinstance(expected_val, datetime):
            actual_val = parse_iso_datetime(actual_val)
        elif capability == 'date' or isinstance(expected_val, date):
            actual_val = parse_iso_date(actual_val)
        elif capability == 'decimal' or isinstance(expected_val, Decimal):
            actual_val = parse_decimal(actual_val)
        elif capability == 'float' or isinstance(expected_val, float):
            actual_val = float(actual_val)
        elif capability == 'integer' or isinstance(expected_val, int):
            actual_val = int(actual_val)
        return actual_val

    def test_11_post_existing_object(self):
        """
        Try POSTing to an existing object.
        """

        res = self.testapp.post_json(
            self._managedTypeUri,
            self._managedSubTypeJSON,
            status=500)
        json_body = res.json_body
        # self.assertTrue('uri' in res_sub.body)
        self.assertEqual(json_body['success'], False)
        self.assertEqual(
            json_body['errors'][0]['value'].lower().find('may not POST'),
            -1,
            "Could not find expected message in error response.")

        return

    def test_14_patch_existing_object(self):
        """
        Try PATCHing an existing object.
        """

        self.assertGreater(len(self.sub_obj_uri_list), 0)
        sub_obj_uri = self.sub_obj_uri_list[0]
        expected_units = 42

        res = self.testapp.patch_json(
            sub_obj_uri,
            {
                "contents": [
                    {
                        "name": "Units",
                        "value": expected_units,
                    }
                ],
            },
            status=200)

        self.assertTrue('uri' in res.text)
        json_body = res.json_body
        self.assertEqual(
            json_body['uri'],
            sub_obj_uri)

        found_units = False
        found_paid = False
        for content in json_body['contents']:
            if content['name'] == "Units":
                actual_val = self.coerce_values(
                    content['value'],
                    expected_val=expected_units,
                    capability=content.get("capability", None))
                self.assertEqual(actual_val, expected_units)
                found_units = True
            elif content['name'] == "Paid":
                self.assertEqual(content['value'], False)
                found_paid = True

        self.assertTrue(found_units)
        self.assertTrue(found_paid)

        # Re-fetch the object and verify the values
        new_res = self.testapp.get(
            sub_obj_uri,
            headers=self._get_json_headers(),
            status=200)
        new_json_body = new_res.json_body
        self.assertEqual(
            new_json_body['uri'],
            sub_obj_uri)
        # log.info("Result: %s", res)

        found_units = False
        found_paid = False
        for content in new_json_body['contents']:
            if content['name'] == "Units":

                actual_val = self.coerce_values(
                    content['value'],
                    expected_val=expected_units,
                    capability=content.get("capability", None))
                self.assertEqual(actual_val, expected_units)
                found_units = True
            elif content['name'] == "Paid":
                self.assertEqual(content['value'], False, 'boolean')
                found_paid = True

        self.assertTrue(found_units)
        self.assertTrue(found_paid)

        return

    def test_20_put_managed_objects(self):
        """
        Test PUT (create or update) objects.
        """
        object_name = "PUT Test"
        expected_uri = 'http://localhost' + self._managedCollectionUri + '/' + quote(object_name)
        self.obj_uri_list.append(expected_uri)

        expected_flux_rate = Decimal("192.0000001")
        expected_active = True
        expected_note = u("Note for PUT item")
        expected_boop_count = 142
        expected_boop_rate = 196.25
        expected_expire_date = date(2024, 6, 12)
        expected_active_datetime = utc_now()
        expected_active_datetime.replace(2014, 6, 12)
        expected_results = {
            self._managedFieldName_FluxRate: expected_flux_rate,
            self._managedFieldName_Active: expected_active,
            self._managedFieldName_Note: expected_note,
            self._managedFieldName_BoopCount: expected_boop_count,
            self._managedFieldName_BoopRate: expected_boop_rate,
            self._managedFieldName_ExpireDate: expected_expire_date,
            self._managedFieldName_ActiveDateTime: expected_active_datetime,
        }

        obj_dict = dict(
            name=object_name,
            resource_type="managed_object",
            contents=self.expand_values_dict(expected_results),
        )
        obj_encoded = self._render_json(obj_dict)

        # make sure it doesn't exist yet
        self.testapp.get(
            expected_uri,
            headers=self._get_json_headers(),
            status=404)

        # now put it
        res = self.testapp.put(
            self._managedCollectionUri,
            obj_encoded,
            content_type=JSON_MIME,
            status=200)
        json_body = res.json_body
        self.assertEqual(
            json_body['uri'],
            expected_uri)
        self.validate_content_values(json_body, expected_results)

        # Now change some values and put it again, and check that we get our new values
        expected_boop_count = 84
        expected_boop_rate = 99.99
        expected_expire_date = date(2024, 6, 12)
        expected_results[self._managedFieldName_BoopRate] = expected_boop_rate
        expected_results[self._managedFieldName_BoopCount] = expected_boop_count
        expected_results[self._managedFieldName_ExpireDate] = expected_expire_date

        # Don't send all fields on the next PUT.
        new_send_fields = {
            self._managedFieldName_BoopRate: expected_boop_rate,
            self._managedFieldName_BoopCount: expected_boop_count,
            self._managedFieldName_ExpireDate: expected_expire_date,
        }

        obj_dict = dict(
            name=object_name,
            resource_type="managed_object",
            contents=self.expand_values_dict(new_send_fields),
        )
        obj_encoded = self._render_json(obj_dict)

        res = self.testapp.put(
            self._managedCollectionUri,
            obj_encoded,
            content_type=JSON_MIME,
            status=200)
        json_body = res.json_body
        self.assertEqual(
            json_body['uri'],
            expected_uri)
        self.validate_content_values(json_body, expected_results)

        self.assertEqual(
            json_body['resource_type'],
            'managed_object')

        # This will get deleted later on

        # # Now delete it
        # res = self.testapp.delete(
        #     expected_uri,
        #     status=200)
        # self.assertTrue('success' in res.json_body)
        # self.assertEqual(res.json_body['success'], True)
        #
        # # Verify deletion
        # self.testapp.get(
        #     expected_uri,
        #     status=404)

    def test_30_add_new_subtype_def(self):
        """
        Add a new subtype ('Test Run') of 'Test Widget' now that we
        have created some existing values.
        """
        self.testapp.get(
            self._managedSub2TypeUri,
            headers=self._get_json_headers(),
            status=404)

        res = self.testapp.put_json(
            self._managedTypeUri,
            self._managedSubType2JSON,
            status=200)
        self.assertTrue('uri' in res.text)
        json_body = res.json_body
        self.assertEqual(
            json_body['uri'],
            'http://localhost' + self._managedSub2TypeUri)

        self.assertEqual(
            json_body['parent_uri'],
            'http://localhost' + self._managedTypeUri)

        self.assertEqual(
            json_body['name'],
            self._managedSub2TypeName)

        self.assertEqual(
            json_body['resource_type'],
            'managed_type')

        return

    def test_31_verify_subtype_collection(self):
        """
        Verify that existing items gained a new empty 'Test Run' collection.
        """
        obj_uri = self.obj_uri_list[-1]
        new_res = self.testapp.get(
            obj_uri,
            headers=self._get_json_headers(),
            status=200)
        new_json_body = new_res.json_body
        self.assertEqual(
            new_json_body['uri'],
            obj_uri)
        self.assertGreater(
            len(new_json_body['contents']),
            0)

        found_test_run_col = False
        for content in new_json_body['contents']:
            if content['name'] == self._managedSub2TypeName \
                    and content['resource_type'] == "managed_collection":
                found_test_run_col = True
                break

        self.assertTrue(
            found_test_run_col,
            "Couldn't find 'Test Run' collection in existing object.")

        return

    def test_32_put_new_subtype_instances(self):
        """
        Add new 'Test Run' objects to a test widget instance
        """

        container_url = 'http://localhost' + self._managedCollectionUri + \
                        '/' + quote("Sample Widget 1") + "/" + quote(self._managedSub2TypeName)
        object_name = "Test Run Alpha"
        expected_uri = container_url + '/' + quote(object_name)
        expected_date = datetime(2031, 1, 31, tzinfo=UTC)
        expected_results = {
            self._managedSub2FieldName_Passed: True,
            self._managedSub2FieldName_LastRun: expected_date,
        }
        test_run_json = {
            "name": object_name,
            "resource_type": "managed_object",
            "contents": self.expand_values_dict(expected_results)
        }
        obj_encoded = self._render_json(test_run_json)
        new_res = self.testapp.put(
            container_url,
            obj_encoded,
            content_type=JSON_MIME,
            status=200)
        new_json_body = new_res.json_body
        self.assertEqual(
            new_json_body['uri'],
            expected_uri)
        self.assertEqual(
            new_json_body['parent_uri'],
            container_url)
        self.assertGreater(
            len(new_json_body['contents']),
            0)
        self.validate_content_values(new_json_body, expected_results)

    def test_95_delete_managed_object_sub(self):
        """
        Delete an individual managed (sub) object.
        """
        sub_obj_uri = self.sub_obj_uri_list[-1]

        res = self.testapp.delete(
            sub_obj_uri,
            status=200)
        self.assertTrue('success' in res.json_body)
        self.assertEqual(res.json_body['success'], True)

        res = self.testapp.get(
            sub_obj_uri,
            headers=self._get_json_headers(),
            status=404)
        log.info("Search object after delete result: %s", res)

    def test_96_delete_managed_object(self):
        """
        Delete an individual managed object.
        """
        obj_uri = self.obj_uri_list[-1]

        res = self.testapp.delete(
            obj_uri,
            status=200)
        self.assertTrue('success' in res.json_body)
        self.assertEqual(res.json_body['success'], True)

        res = self.testapp.get(
            obj_uri,
            headers=self._get_json_headers(),
            status=404)
        log.info("Search object after delete result: %s", res)

    def test_99_delete_managed_type(self):
        """
        Clean up the entire set of test data we created by recursively deleting the
        managed type definition.
        """
        res = self.testapp.delete(
            self._managedTypeUri,
            status=200)
        self.assertTrue('success' in res.json_body)
        self.assertEqual(res.json_body['success'], True)

        res = self.testapp.get(
            self._managedTypeUri,
            headers=self._get_json_headers(),
            status=404)
        log.info("Search type after delete result: %s", res)

        res_sub = self.testapp.get(
            self._managedSubTypeUri,
            headers=self._get_json_headers(),
            status=404)
        log.info("Search subtype after delete result: %s", res_sub)
