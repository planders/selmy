# !/usr/bin/env python
# -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-
# vim: fileencoding=utf-8 tabstop=4 expandtab shiftwidth=4

"""
Unit tests for REST API

http://docs.pylonsproject.org/projects/pyramid/en/1.5-branch/narr/testing.html

These tests call the relevant functions directly and do not go through a full
HTTP request.
"""

import unittest

from pyramid import testing

from selmy.models.managed import RootType
from selmy.request import SelmyDummyRequest
from selmy.views.rest import API

__author__ = 'Preston Landers (planders@gmail.com)'
__copyright__ = 'Copyright (c) 2017 Preston Landers'
__license__ = 'BSD'


class TestRestBasics(unittest.TestCase):
    RequestClass = SelmyDummyRequest

    def setUp(self):
        self.config = testing.setUp()

    def tearDown(self):
        testing.tearDown()

    # noinspection PyPep8Naming
    def setup_request(self, path="/", base_url=None, headers=None, POST=None):
        self.request = self.RequestClass(path, base_url=base_url, headers=headers, POST=POST)
        return self.request

    def test_root_collection_view(self):
        from ..views.rest.root import RestRoot

        self.setup_request(path="/" + API)
        real_root = RootType.get_root_instance()
        self.request.context = real_root

        returned_root = RestRoot(self.request).collection_get()

        self.assertEqual(self.request.response.status_int, 200)
        self.assertEqual(real_root, returned_root)
        self.assertEqual(returned_root.name, "")
        self.assertGreater(
            len(list(returned_root.children_plus_nodes(self.request))),
            1)
