# !/usr/bin/env python
# -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-
# vim: fileencoding=utf-8 tabstop=4 expandtab shiftwidth=4

"""
Unit tests

http://docs.pylonsproject.org/projects/pyramid/en/1.5-branch/narr/testing.html
"""

import unittest

from pyramid import testing
from pyramid.paster import setup_logging

from selmy import get_default_config
from selmy.i18n import _
from selmy.request import SelmyDummyRequest
from selmy.util import u

__author__ = 'Preston Landers (planders@gmail.com)'
__copyright__ = 'Copyright (c) 2017 Preston Landers'
__license__ = 'BSD'

DEFAULT_LOCALE = 'en'

RequestClass = SelmyDummyRequest


TEST_USERS = [
    {
        "username": u("test_user"),
        "password": u("asdf"),
        "active": u("Y"),
    },
]


class SelmyTestCase(unittest.TestCase):

    # Probably a better way to do this...
    USE_LOGGING_CONFIG = False

    @classmethod
    def setUpClass(cls):
        super(SelmyTestCase, cls).setUpClass()
        if cls.USE_LOGGING_CONFIG:
            setup_logging(get_default_config())

    def setUp(self):
        self.config = testing.setUp()

    def tearDown(self):
        testing.tearDown()


class TestMyViewSuccessCondition(SelmyTestCase):

    def test_passing_view(self):
        from ..views.home import home_view
        # This is a meaningless test of a do-nothing view
        request = RequestClass(path="/")
        info = home_view(request)
        self.assertEqual(info['project'], 'Selmy')
        self.assertGreater(len(info['copyright']), 1)

    def test_hello_view(self):
        from ..views.hello import hello_view

        request = RequestClass(path="/hello")
        x_t = 'Hello, world. Secret: 99'  # Assumes locale=en
        info = hello_view(request)
        self.assertEqual(info['project'], 'Selmy')
        self.assertEqual(info['x'], x_t)


class TestMyViewFailureCondition(SelmyTestCase):

    def test_failing_api_view(self):
        from ..views.rest.root import RestRoot

        request = RequestClass(path="/api/doesnotexist")
        print("Request user: %s", request.user)
        request._LOCALE_ = DEFAULT_LOCALE
        # noinspection PyUnusedLocal
        info = RestRoot(request).get()
        self.assertEqual(request.response.status_int, 500)


class TestBootstrap(SelmyTestCase):
    def test_bootstrap(self):
        from selmy import bootstrap
        env = bootstrap()
        self.assertIsNotNone(env)
        self.assertIn("app", env)
        self.assertIn("closer", env)
        self.assertIn("registry", env)
        self.assertIn("request", env)
        self.assertIn("root", env)
        root = env['root']
        self.assertIsNotNone(root)

    def test_make_localizer(self):
        from selmy.i18n import make_localizer
        localizer = make_localizer()
        tr = localizer.translate
        test_str = "Test string"
        result_str = tr(_("Test string"))  # TODO: says missing domain param!
        self.assertEqual(test_str, result_str)
        return


# Don't need to import other tests under this package here
# if you use --all-modules option with nose
