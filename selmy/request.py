#!/usr/bin/env python
# -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-
# vim: fileencoding=utf-8 tabstop=4 expandtab shiftwidth=4

"""
Selmy request factory. Includes request.localize_datetime().
"""

from logging import getLogger

from pyramid.request import Request
from pyramid.testing import DummyRequest

from selmy.i18n import localize_datetime, localize_number
from selmy.request_props import register_dummy_request_properties

__author__ = 'Preston Landers (planders@gmail.com)'
__copyright__ = 'Copyright (c) 2017 Preston Landers'
__license__ = 'BSD'

log = getLogger(__name__)


class SelmyRequest(Request):
    """
    Base class for all requests in Selmy.

    http://docs.pylonsproject.org/docs/pyramid/en/latest/narr/hooks.html#changing-the-request-factory

    TODO: localize_date() and other types?
    * date
    * numbers
    * currency?

    http://babel.edgewall.org/wiki/Documentation/0.9/dates.html
    """

    def localize_number(self, *args, **kwargs):
        """
        Localize a datetime object with some defaults set. See :py:func:`selmy.i18n.localize_number`
        """
        return localize_number(self, *args, **kwargs)

    def localize_datetime(self, *args, **kwargs):
        """
        Localize a datetime object with some defaults set. See
        :py:func:`selmy.i18n.localize_datetime`
        """
        return localize_datetime(self, *args, **kwargs)


class SelmyDummyRequest(DummyRequest):
    """
    Dummy request for testing purposes.
    """

    def __init__(
            self, params=None, environ=None, headers=None, path='/', cookies=None, post=None, **kw):
        super(SelmyDummyRequest, self).__init__(params, environ, headers, path, cookies, post, **kw)

        # Check kwargs for this instead?
        self._LOCALE_ = "en"

        # Register the same custom properties as for a real request
        register_dummy_request_properties(self)

    def localize_number(self, *args, **kwargs):
        """
        Localize a datetime object with some defaults set. See :py:func:`selmy.i18n.localize_number`
        """
        return localize_number(self, *args, **kwargs)

    def localize_datetime(self, *args, **kwargs):
        """
        Localize a datetime object with some defaults set. See
        :py:func:`selmy.i18n.localize_datetime`
        """
        return localize_datetime(self, *args, **kwargs)
