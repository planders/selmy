# !/usr/bin/env python
# -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-
# vim: fileencoding=utf-8 tabstop=4 expandtab shiftwidth=4

"""
Internationalization (i18n) support for Selmy.

See README.md for guidance on how to keep the code internationalized as you develop.

To get localized strings in a command line program, use selmy.bootstrap() to get an
environment, then use env['request'].localizer.translate()

TODO: localize currency?
"""

import locale
import os
from decimal import Decimal
from logging import getLogger

import pyramid.i18n
from babel.dates import format_datetime
from babel.numbers import format_decimal, format_number
from tzlocal import get_localzone

from selmy.util import utc_now

__author__ = 'Preston Landers (planders@gmail.com)'
__copyright__ = 'Copyright (c) 2017 Preston Landers'
__license__ = 'BSD'

log = getLogger(__name__)

# Our general purpose translation string factory - use this throughout the app
_ = pyramid.i18n.TranslationStringFactory('selmy')


# noinspection PyShadowingBuiltins,PyShadowingNames
def localize_number(request, number, format=None, locale=None):
    """
    Localize a number including Decimal types.

    :param request: Selmy (Pyramid) request
    :type request: SelmyRequest
    :param number: the number to format
    :type number: number type
    :param format: override the numeric format (see Babel docs)
    :type format: str
    :param locale: the locale to use, gets from the request if None
    :type locale: str
    :return: localized number
    :rtype: number
    """
    if locale is None:
        locale = request.localizer.locale_name
    if isinstance(number, Decimal):
        return format_decimal(number, format=format, locale=locale)
    return format_number(number, locale=locale)


# noinspection PyShadowingBuiltins,PyShadowingNames
def localize_datetime(request, datetime=None, format='medium', tzinfo=None, locale=None):
    """
    Localize a datetime object with some defaults set.

    :param request: Selmy request
    :type request: SelmyRequest
    :param datetime: the datetime to localize, or now if None
    :type datetime: datetime
    :param format: override format string
    :type format: str
    :param tzinfo: override timezone (defaults to system local zone)
    :type tzinfo: datetime.tzinfo
    :param locale: override locale (defaults to request locale)
    :type locale: str
    :return: a localized datetime.
    :rtype: str
    """
    if datetime is None:
        datetime = utc_now()
    if tzinfo is None:
        # TODO: this is the SERVER's local zone, but we should have a preference for the user's
        # preferred zone...
        tzinfo = get_localzone()
    if locale is None:
        locale = request.localizer.locale_name
    return format_datetime(datetime=datetime, format=format, tzinfo=tzinfo, locale=locale)


def locale_negotiator(request):
    """
    Determine what to set as the request locale.
    """
    if not hasattr(request, '_LOCALE_'):
        if 'LANG' in os.environ:
            user_locale = os.environ['LANG']
        else:
            user_locale = request.accept_language.best_match(
                ('en', 'fr', 'de'),
                'en')
        request._LOCALE_ = user_locale
        # request._LOCALE_ = 'de'  # force it here...

    # noinspection PyProtectedMember
    res = request._LOCALE_
    # log.debug("negotiated locale: %s", res)
    return res


def make_localizer(user_locale=None):
    """
    Get a localizer suitable for use outside of a normal
    pyramid request, such as from the command line.

    Normally you should just use bootstrap and get a localizer from
    that.  But in certain rare cases (e.g, help text on selmy console)
    we need to get a localizer even before we bootstrap.

    :param str user_locale: force a particular locale (e.g. "fr_FR")
    :return: a localizer for translating strings
    :rtype: localizer
    """
    if user_locale is None:
        if os.name == 'nt':
            user_locale = os.environ.get("LANG", None)
            if not user_locale:
                user_locale = locale.getdefaultlocale()[0]
        else:
            user_locale = locale.getdefaultlocale()[0]
        log.debug("make_localizer user locale is: %s", user_locale)
    here = os.path.dirname(__file__)
    localedir = [os.path.join(here, 'locale'), ]
    # TODO: the translate function from this requires a domain parameter...?
    return pyramid.i18n.make_localizer(user_locale, localedir)
