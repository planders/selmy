# !/usr/bin/env python
# -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-
# vim: fileencoding=utf-8 tabstop=4 expandtab shiftwidth=4

"""
Constant definitions for Selmy.
"""

__author__ = 'Preston Landers (planders@gmail.com)'
__copyright__ = 'Copyright (c) 2017 Preston Landers'
__license__ = 'BSD'

# Duplicated in setup.py...
SELMY_VERSION = '0.0.2'

#: Root of the REST API relative to site root (assumed leading /)
API = "api"
