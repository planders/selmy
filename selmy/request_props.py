# !/usr/bin/env python
# -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-
# vim: fileencoding=utf-8 tabstop=4 expandtab shiftwidth=4

"""
Properties for the 'request' object.
"""

from logging import getLogger

from apex.models import AuthID
from six.moves.urllib_parse import unquote

from selmy.constants import SELMY_VERSION
from selmy.flash_messages import get_flash_message_list
# noinspection PyProtectedMember
from selmy.i18n import _
from selmy.util import repr_auth_user

__author__ = 'Preston Landers (planders@gmail.com)'
__copyright__ = 'Copyright (c) 2017 Preston Landers'
__license__ = 'BSD'

log = getLogger(__name__)


def register_request_properties(config):
    """
    Register the custom request properties provided by Selmy.

    :param config: Pyramid config object
    :type config: Configurator
    :return: the modified config object
    :rtype: Configurator
    """
    # Note: reify essentially means that it's computed once and then cached
    # for the lifetime of the (request) object
    config.add_request_method(get_user, 'user', reify=True)
    config.add_request_method(get_user_label, 'user_label', reify=True)
    config.add_request_method(product_config, 'product_config', reify=True)
    config.add_request_method(get_flash_message_list, 'flash_message_list', reify=False)
    config.add_request_method(theme_url, 'theme_url', reify=True)
    return config


def register_dummy_request_properties(dummyRequest):
    """
    Register our custom request properties onto a DummyRequest instance
    for testing purposes.

    :param dummyRequest: dummy request instance
    :type dummyRequest: SelmyDummyRequest
    :return: None
    """
    dummyRequest.set_property(get_user, "user", reify=True)
    dummyRequest.set_property(product_config, "product_config", reify=True)
    dummyRequest.set_property(theme_url, reify=True)
    dummyRequest.set_property(get_flash_message_list, reify=True)


def get_user(request):
    """
    This controls what request.user returns. None if we are not
    logged in, otherwise the apex AuthUser.

    :param request: pyramid request
    :type request: Request
    :return: AuthUser object if authenticated, otherwise None
    :rtype: AuthUser
    """
    auth_user_id = getattr(request, "authenticated_userid", None)
    if not auth_user_id:
        return None
    auth_id = AuthID.get_by_id(auth_user_id)
    # TODO: support multiple accounts here?
    if len(auth_id.users) > 1:
        log.warning("AuthId %s has more than one AuthUser! %s", auth_id, auth_id.users)
    auth_user = auth_id.users[0]
    return auth_user


def get_user_label(request):
    user = request.user
    if user:
        if user.profile and user.profile.name:
            return user.profile.name
        if user.login:
            return user.login

        log.warning(
            "Warning: can't find user name for logged in user: %s" % (repr_auth_user(user)))
    fallback = "(Unknown name)"
    if request.product_config and 'unknown_username' in request.product_config:
        return request.product_config['unknown_username']
    return fallback


class ProductConfig(object):
    # Need a better way to do this...
    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)


def product_config(request):
    """
    Returns the "product config" dict with general configuration properties
    TODO: this is extremely rudimentary, think about architecture
    Figure out a more comprehensive system for tracking these types of things.

    :param request:
    :type request: Request
    :return: the translated product name
    :rtype: str or Unicode
    """
    pc = ProductConfig(**{
        "product_name": request.localizer.translate(_("Selmy")),
        # "product_version": "1.0",
        "selmy_version": SELMY_VERSION,
        "unknown_username": "(Name missing)",
    })
    return pc


def theme_url(request):
    """
    Get the user's current theme URL.

    :param request:
    :type request: Request
    :return: theme URL
    :rtype: str or Unicode
    """
    theme = request.cookies.get('selmy_theme_css', None)
    if theme is None:
        theme = request.static_url('selmy:static/css/themes/bootstrap.cerulean.min.css')
    else:
        theme = unquote(theme)
    return theme
