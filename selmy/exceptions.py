# !/usr/bin/env python
# -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-
# vim: fileencoding=utf-8 tabstop=4 expandtab shiftwidth=4


"""
Selmy exceptions.
"""

__author__ = 'Preston Landers (planders@gmail.com)'
__copyright__ = 'Copyright (c) 2017 Preston Landers'
__license__ = 'BSD'

SVNID = '$Id$'


class RequiredValueError(ValueError):
    """Raised when the API was expected a key or other item in the input and didn't get it."""
    pass
