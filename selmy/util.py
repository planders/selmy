#!/usr/bin/env python
# -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-
# vim: fileencoding=utf-8 tabstop=4 expandtab shiftwidth=4

"""
General utilities.
"""

import decimal
from datetime import datetime, date

import dateutil.parser
import simplejson
from pygments import highlight
from pygments.formatters.html import HtmlFormatter
from pygments.lexers.javascript import JavascriptLexer
from pyramid.renderers import render, JSON
from pytz import UTC
from six import PY2
from six.moves.urllib_parse import unquote_plus

__author__ = 'Preston Landers (planders@gmail.com)'
__copyright__ = 'Copyright (c) 2017 Preston Landers'
__license__ = 'BSD'


def u(s):
    """
    Backwards compatibility for Python 3 not having unicode()
    :param s: a string-like thing
    :return: unicode object (Python 2) or a str object (Python 3)
    """
    if PY2:
        # noinspection PyCompatibility,PyUnresolvedReferences
        return unicode(s)
    else:
        return str(s)


def get_full_class_name(a_cls):
    return a_cls.__module__ + "." + a_cls.__name__


def single_quote(a_str):
    a_str = a_str.replace("'", r"\'")
    return "'%s'" % (a_str,)


def repr_path(resource):
    rpath = resource.get_path_name()
    rpath = unquote_plus(rpath)
    return single_quote(rpath)


def utc_now():
    """
    Return the current UTC time in a timezone-aware object, suitable for storing
    in the database

    :return: current time in UTC zone
    :rtype: datetime
    """
    return datetime.now(UTC)


# noinspection PyUnusedLocal
def json_datetime_adapter(obj, request):
    """
    Convert a datetime.datetime object to JSON.

    :param obj: a DateTime instance
    :type obj: datetime.datetime
    :param request: current request
    :type request: SelmyRequest
    :return: JSON representation
    :rtype: str
    """
    return obj.isoformat()


# noinspection PyUnusedLocal
def json_date_adapter(obj, request):
    """
    Convert a datetime.date object to JSON.

    :param obj: a Date instance
    :type obj: datetime.date
    :param request: current request
    :type request: SelmyRequest
    :return: JSON representation
    :rtype: str
    """
    return obj.isoformat()


# noinspection PyUnusedLocal
def json_decimal_adapter(obj, request):
    return simplejson.dumps(obj, use_decimal=True)


def add_json_adapters(json_renderer):
    json_renderer.add_adapter(date, json_date_adapter)
    json_renderer.add_adapter(datetime, json_datetime_adapter)
    json_renderer.add_adapter(decimal.Decimal, json_decimal_adapter)


def get_json_renderer():
    """Get a customized JSON renderer."""
    json_renderer = JSON(indent=4)
    add_json_adapters(json_renderer)
    return json_renderer


def parse_iso_date(iso_date):
    """Parse an ISO 8601 string into a date.

    (Note... does not coerce to date, could get datetime!)
    """
    ret = dateutil.parser.parse(iso_date)
    return date(ret.year, ret.month, ret.day)


def parse_iso_datetime(iso_datetime):
    """Parse an ISO 8601 string into a datetime."""
    return dateutil.parser.parse(iso_datetime)


def parse_decimal(dec_str):
    """Parse a string into a Decimal number."""
    return decimal.Decimal(dec_str)


def highlight_js_to_html(js_thing):
    return highlight(js_thing, JavascriptLexer(), HtmlFormatter())


def dumps(thing):
    # Better way to do this? second argument seems to be ignored?
    # Can put "request": request in there ... to change the response content-type
    # which we don't actually want to do here.
    return render("json", thing)


def strip_trailing_slash(a_str):
    if a_str[-1] == "/":
        return a_str[:-1]
    return a_str


def repr_auth_user(au):
    return '<AuthUser Login:%r Email:%r>' % (au.login, au.email)
