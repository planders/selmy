# !/usr/bin/env python
# -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-
# vim: fileencoding=utf-8 tabstop=4 expandtab shiftwidth=4

#
# Copyright (c) 2017 Preston Landers
# Licensed Materials - All Rights Reserved
#

"""
Authentication related code

We are using apex/velruse but there is some selmy specific auth code.
"""

from logging import getLogger

__author__ = 'Preston Landers (planders@gmail.com)'
__copyright__ = 'Copyright (c) 2017 Preston Landers'
__license__ = 'BSD'

log = getLogger(__name__)


# noinspection PyUnusedLocal
def check_credentials(username, password, request):
    """
    Basic username/password check for the internal PW database.

    :param username: login name
    :param password: password
    :param request: pyramid request (unused here)
    :return:
    """
    from apex.models import AuthUser

    if not AuthUser.check_password(
            login=username, password=password):
        log.info('Login failed for %s' % (username,))
        return None

    # an empty list is enough to indicate logged-in... watch how this affects the principals
    # returned in the home view if you want to expand ACLs later
    log.info('Login succeeded for %s' % (username,))
    return []
