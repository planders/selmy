webpackJsonp([1],{

/***/ 11:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_jquery__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_jquery___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_jquery__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_bootstrap__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_bootstrap___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_bootstrap__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_bootstrap_css__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_bootstrap_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_bootstrap_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_selmy_css__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_selmy_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_selmy_css__);





// sets up a JS namespace called selmy
// noinspection JSUnusedLocalSymbols
window.selmy = (function (selmy, $, window, undefined) {

    $(document).ready(function () {
        const bootswatch_theme = $("#bootswatch-theme");
        const theme_cookie_name = "selmy_theme_css";

        if ($.cookie(theme_cookie_name)) {
            const curTheme = bootswatch_theme.attr("href");
            const cookieTheme = $.cookie(theme_cookie_name);
            if (curTheme !== cookieTheme) {
                bootswatch_theme.attr("href", cookieTheme);
            }
        }

        // THEME selector
        $("#mainMenu").find(".theme-dropdown-menu li a").click(function () {
            bootswatch_theme.attr("href", $(this).attr('rel'));
            $.cookie("selmy_theme_css", $(this).attr('rel'), {expires: 365, path: '/'});
            return false;
        });

        window.selmy.FBUSERID = null;
        window.selmy.FBTOKEN = null;
        window.selmy.FBNOTAUTH = null;
        window.selmy.FBSTATUS = 'You are not logged into FB.';
        window.selmy.FBLoginStatusCallback = function FBLoginStatusCallback(response) {
            if (response.status === 'connected') {
                // the user is logged in and has authenticated your
                // app, and response.authResponse supplies
                // the user's ID, a valid access token, a signed
                // request, and the time the access token
                // and signed request each expire
                const uid = response.authResponse.userID;
                const accessToken = response.authResponse.accessToken;
                // window.alert("You are logged into FB and authenticated this app.");
                window.selmy.FBUSERID = uid;
                window.selmy.FBTOKEN = accessToken;
                window.selmy.FBSTATUS = 'You are logged into FB and have authenticated this app.';
            } else if (response.status === 'not_authorized') {
                // the user is logged in to Facebook,
                // but has not authenticated your app
                // window.alert("You are logged into FB but have not authenticated this app.");
                window.selmy.FBSTATUS = 'You are logged into FB but have not authenticated this app.';
                window.selmy.FBNOTAUTH = true;
            } else {
                // the user isn't logged in to Facebook.
                // window.alert("You are not logged into FB");
            }
            $(".social-buttons").css("opacity", "1");

            if (selmy.SocialPageFBReady) {
                selmy.SocialPageFBReady();
            }
        };
        window.FBLoginStatusCallback = selmy.FBLoginStatusCallback;

        $.ajaxSetup({cache: true});

//		if ((window['USE_FB'] != undefined) && USE_FB) {
//			$.getScript('//connect.facebook.net/en_US/all.js', function(){
//				FB.init({
//				  appId: '194168720763795',
//				  channelUrl: '//connexus-apt.appspot.com/channel.html', // Channel file for x-domain comms
//				  status     : true,                                 // Check Facebook Login status
//				  xfbml      : true                                  // Look for social plugins on the page
//				});
//
//				$('#loginbutton,#feedbutton').removeAttr('disabled');
//			    FB.getLoginStatus(selmy.FBLoginStatusCallback);
//			});
//		}


        // Activate tooltip and popover features
        $("[rel='tooltip']").tooltip();
        $("[rel='popover']").popover(
            {
                container: 'body',
                html: true,
                content: function () {
                    return $(this).next('.popper-content').html();
                }
            });

        // When uploading a file, automatically set the comment to the filename
//		$("#uploadMediaFile").change(function() {
//
//			// Choose a default comment based on the filename
//			selmy.extractCommentFromFilename = function extractCommentFromFilename(fileInput) {
//				var fnParts = $(fileInput).val().split("\\");
//				var rv = fnParts[fnParts.length-1]; // select base filename out of path
//				var arr = /^(.*)[.]\w{3,4}$/g.exec(rv);  // remove any 3 or 4 letter extensions
//				rv = arr[1];
//				return rv;
//			};
//
//			$("#uploadMediaComments").val(
//					selmy.extractCommentFromFilename(this))
//					.select().focus();
//		});
//
//		$("#uploadButton").click(function() {
//			if (!$("#uploadMediaFile").val()) {
//				return false;
//			}
//		});

    });

    return selmy;
})(window.selmy || {}, __WEBPACK_IMPORTED_MODULE_0_jquery___default.a, window);


/***/ }),

/***/ 12:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })

},[11]);