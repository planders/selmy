import 'selmy.css';
import Vue from 'vue';
import VueRouter from 'vue-router';
import selmy_object_manager from '../components/manage/manage.vue'

Vue.use(VueRouter);

// views as components
// const mainView = Vue.component({
//     template: `
//     <p>Main view content here.</p>
//   `,
//     components: [manageComponent],
// });
const mainView = {
    template: `
    <div>
        <p>Main view content here.</p>
        <selmy-object-manager></selmy-object-manager>
    </div>
  `,
    components: {'selmy-object-manager': selmy_object_manager},
};

// const aboutView = Vue.component({
//     template: `
//     <p>About view content.</p>
//   `
// });
const aboutView = {
    template: `
    <p>About view content.</p>
  `
};

const routes = [
    {path: '/', component: mainView, name: "main"},
    {path: '/about', component: aboutView, name: "about"}
];

// create router
const router = new VueRouter({
    routes // short for `routes: routes`
});


// kick off!
const app = new Vue({
    router
}).$mount('#manage-app');
