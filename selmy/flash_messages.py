# !/usr/bin/env python
# -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-
# vim: fileencoding=utf-8 tabstop=4 expandtab shiftwidth=4


"""
Flash message support.
"""

from logging import getLogger

__author__ = 'Preston Landers (planders@gmail.com)'
__copyright__ = 'Copyright (c) 2017 Preston Landers'
__license__ = 'BSD'

log = getLogger(__name__)


class FlashMessage(object):
    """
    Flash Message class which allows you to set the type to one of:
      * SUCCESS
      * ERROR
      * INFO
      * WARNING

    Exposes the following properties used in flash_messages.jinja2:
      * message  (string, can contain HTML)
      * css_class    (CSS class for the alert container)
      * icon_class   (CSS class of the relevant icon)

    Use like:

    >>> import selmy.request
    >>> request = selmy.request.SelmyDummyRequest()
    >>> request.session.flash(FlashMessage("Core meltdown!", FlashMessage.ERROR))
    >>> msg_list = get_flash_message_list(request)
    >>> msg_list
    [FlashMessage<error: Core meltdown!>]
    >>> for msg in msg_list:
    ...     assert len(msg.css_class)
    ...     assert len(msg.icon_class)

    The message string can be any object which has a sensible str() output.
    """
    STATUS = SUCCESS = 'success'
    ERROR = 'error'
    INFO = 'info'
    WARNING = 'warning'

    ALL_TYPES = [SUCCESS, ERROR, INFO, WARNING]

    DEFAULT_ICON = "glyphicon glyphicon-ok-sign"
    ICONS = {
        SUCCESS: DEFAULT_ICON,
        ERROR: "glyphicon glyphicon-remove",
        INFO: "glyphicon glyphicon-info-sign",
        WARNING: "glyphicon glyphicon-warning-sign",
    }

    DEFAULT_CSS_CLASS = "alert-success"
    CSS_CLASS = {
        SUCCESS: DEFAULT_CSS_CLASS,
        ERROR: "alert-danger",
        INFO: "alert-info",
        WARNING: "alert-warning",
    }

    def __init__(self, message, msg_type=SUCCESS):
        self.message = message
        self.msg_type = msg_type

    @property
    def icon_class(self):
        return self.ICONS.get(self.msg_type, self.DEFAULT_ICON)

    @property
    def css_class(self):
        return self.CSS_CLASS.get(self.msg_type, self.DEFAULT_CSS_CLASS)

    def __repr__(self):
        return "FlashMessage<%s: %s>" % (self.msg_type, self.message)


def get_flash_message_list(request):
    """
    Returns a list of :py:class::`FlashMessage` (aka status/error messages)

    TODO: this is not the final way this should work. What flash
    message libraries are there??

    :param request:
    :type request: Request
    :return: flash message list
    :rtype: list of FlashMessage
    """
    res = []
    # return [FlashMessage("Testing Error", msg_type=FlashMessage.ERROR)]
    for fm in request.session.pop_flash():
        if not isinstance(fm, FlashMessage):
            fm = FlashMessage(fm, msg_type=FlashMessage.SUCCESS)
        res.append(fm)
    log.debug("Getting flash messages for user: %s -- %r", request.user, res)
    return res
