# !/usr/bin/env python
# -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-
# vim: fileencoding=utf-8 tabstop=4 expandtab shiftwidth=4

# #
# # Copyright (c) 2017 Preston Landers
# # Licensed Materials - All Rights Reserved
# #

"""
Selmy is a WSGI application for business data management.
"""

import os
from logging import getLogger

# noinspection PyUnresolvedReferences
import concurrent_log_handler
import pyramid.paster
from pyramid.authentication import BasicAuthAuthenticationPolicy
from pyramid.config import Configurator
from sqlalchemy import engine_from_config

from selmy.auth import check_credentials
from selmy.i18n import locale_negotiator
from selmy.models import Base, DBSession, root_factory
from selmy.request import SelmyRequest
from selmy.request_props import register_request_properties
from selmy.util import get_json_renderer

try:
    from typing import List
except ImportError:
    List = None

__author__ = 'Preston Landers (planders@gmail.com)'
__copyright__ = 'Copyright (c) 2017 Preston Landers'
__license__ = 'BSD'

log = getLogger(__name__)


# noinspection PyUnusedLocal
def db(pyramid_request):
    return DBSession


# noinspection PyUnusedLocal
def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    settings = dict(settings)
    app_prefix = settings.get('app_prefix', '')
    settings["app_prefix"] = app_prefix
    settings.setdefault('jinja2.i18n.domain', 'selmy')
    settings.setdefault('jinja2.newstyle', 'true')  # new style translation strings (gettext)
    config = Configurator(settings=settings, root_factory=root_factory, route_prefix=app_prefix)

    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    Base.metadata.bind = engine
    config.add_request_method(db, 'db', reify=True)

    config.set_request_factory(SelmyRequest)
    config.add_translation_dirs('locale/')
    config.set_locale_negotiator(locale_negotiator)
    config.include('pyramid_jinja2')
    config.include('pyramid_exclog')

    config.include('velruse.providers.google_oauth2')
    config.include('apex', route_prefix='/auth')

    set_auth_policy(settings, config)

    # cornice exceptions conflicts with WebError despite different route_prefix
    config.add_settings(handle_exceptions=False)
    config.add_view_predicate('content_type', ContentTypePredicate)

    register_request_properties(config)

    # Cornice doesn't seem to understand route_prefix option...
    # https://github.com/mozilla-services/cornice/commit/9fa57e86f9ae5a25cb3c36ffb6506c5c4866f4d7
    config.include('cornice')

    config.add_renderer('json', get_json_renderer())

    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('home', '/')
    config.add_route('hello', '/hello')
    config.add_route('manage', '/manage')

    config.add_route('protected', '/protected')

    config.add_route('browse_noslash', '/browse')
    config.add_route('browse', '/browse*traverse')

    config.add_route('test', '/test')

    config.scan()
    return config.make_wsgi_app()


class ContentTypePredicate(object):
    """
    TODO: move
    https://stackoverflow.com/questions/19783896/how-do-you-have-different-views-to-serve-different-content-types-in-pyramid
    """

    # noinspection PyUnusedLocal
    def __init__(self, val, config):
        self.val = val

    def text(self):
        return 'content type = %s' % self.val

    phash = text

    def __call__(self, context, this_request):
        return this_request.content_type == self.val


# noinspection PyShadowingNames
def bootstrap(config_uri=None, request=None, options=None):
    """
    Bootstrap a Selmy request environment suitable for command line use. You can use this instead
    of pyramid.paster.bootstrap to create an environment where you can do things like generate
    URLs, translate strings, and access the data model outside of a normal web request.

    In order to bootstrap we need to know which config .ini file to use. If the config_uri
    parameter is not set, first we look at SELMY_CONFIG in the environment. Otherwise we look for
    config files in this order in the parent directory of the selmy package::

        local.ini
        development.ini
        production.ini

    :param config_uri: The configuration .ini file to use
    :param request: optional request object
    :param options: extra options dict
    :return: the Pyramid environment dict
    """
    if not config_uri:
        config_uri = get_default_config()
    res = pyramid.paster.bootstrap(config_uri, request=request, options=options)

    # Ensure that loggers created after calling this will be properly configured
    # TODO: enable?
    # pyramid.paster.setup_logging(config_uri)

    return res


def get_default_config():
    config_uri = os.environ.get("SELMY_CONFIG", None)
    if not config_uri:
        here = os.path.dirname(__file__)
        files_to_check = [
            os.path.normpath(os.path.join(here, '..', 'local.ini')),
            os.path.normpath(os.path.join(here, '..', 'development.ini')),
            os.path.normpath(os.path.join(here, '..', 'production.ini')),
        ]
        for check_file in files_to_check:
            if os.path.exists(check_file):
                config_uri = check_file
                break
    return config_uri


def set_auth_policy(settings, config):
    """Combine basic auth (if enabled) with Apex.

    Basic auth is possibly a temporary expedient to test the API"""
    from apex import ApexAuthSecret
    from apex.lib.libapex import groupfinder
    from pyramid.authentication import AuthTktAuthenticationPolicy
    from pyramid_multiauth import MultiAuthenticationPolicy
    from pyramid.interfaces import IAuthenticationPolicy

    if 'apex.auth_secret' not in settings:
        raise ApexAuthSecret()

    policies = []  # type: List[IAuthenticationPolicy]
    allow_basic_auth = settings.get("selmy.allow_basic_auth", False)
    if allow_basic_auth:
        basic_authn_policy = BasicAuthAuthenticationPolicy(check_credentials)
        policies.append(basic_authn_policy)

    apex_authn_policy = AuthTktAuthenticationPolicy(
        settings.get('apex.auth_secret'),
        hashalg='sha512',
        callback=groupfinder)
    policies.append(apex_authn_policy)

    combined_authn_policy = MultiAuthenticationPolicy(policies)

    config.set_authentication_policy(combined_authn_policy)
