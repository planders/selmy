# !/usr/bin/env python
# -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-
# vim: fileencoding=utf-8 tabstop=4 expandtab shiftwidth=4

"""
Selmy database initialization.
"""

import getpass
import os
import sys

import transaction
from pyramid.paster import get_appsettings, setup_logging
from pyramid.scripts.common import parse_vars
from sqlalchemy import engine_from_config

from selmy.models import (
    DBSession,
    Base,
    get_id_maker)
from selmy.models.managed import ManagedObjectField
from selmy.models.managed.managed_field_data_type import ManagedFieldDataType, ManagedDecimalValue
from selmy.models.virtual_node import SystemTypeDefinition


def usage(argv):
    cmd = os.path.basename(argv[0])
    print('usage: %s <config_uri> [var=value]\n'
          '(example: "%s development.ini")' % (cmd, cmd))
    sys.exit(1)


get_id = get_id_maker()


def get_default_username():
    return getpass.getuser()


def add_default_records():
    """
    Create some default records to populate a usable database instance.
    :return:
    """
    # A simple managed domain type called "Project"
    from selmy.models.managed import ManagedType, ManagedField, ManagedObject, RootType

    # SystemTypeDefinition.add_system_default_records(DBSession)
    SystemTypeDefinition.add_system_default_records()
    root_type = RootType.get_root_instance()

    # projects = ManagedType(name="Project", description="Company projects and things.",
    #                        parent_id=None)
    projects = ManagedType.create(
        name="Project", description="Company projects and things.",
        parent_id=None)
    DBSession.add(projects)
    DBSession.flush()
    projects.ensure_collections()  # TODO

    # projects_collection = projects.create_collection(DBSession)
    projects_collection = root_type['Project']

    # text_type = ManagedFieldDataType.get_type_by_capability(ManagedFieldDataType.UNICODE)
    decimal_type = ManagedFieldDataType.get_type_by_capability(ManagedFieldDataType.DECIMAL)
    # selection_type = ManagedFieldDataType.get_type_by_capability(ManagedFieldDataType.SELECTION)

    # p_name = ManagedField(order=1, name="Name", description="Project Name", parent_id=projects.id,
    # data_type_id=text_type.id)
    # p_description = ManagedField(order=2, name="Description", description="Project description",
    # parent_id=projects.id,
    # data_type_id=text_type.id)
    # p_owner = ManagedField(order=3, name="Owner", description="Project Owner",
    #                        parent_id=projects.id,
    # data_type_id=selection_type.id)
    p_pay_rate = ManagedField.create(
        order=4, name="Pay Rate", description="Project pay rate", parent_id=projects.id,
        data_type_id=decimal_type.id)

    # project_fields = [p_name, p_description, p_owner, p_pay_rate]
    project_fields = [p_pay_rate]
    DBSession.add_all(project_fields)
    DBSession.flush()

    # Now add a project and some field values.
    project_one = ManagedObject.create(
        name="Project One",
        parent_id=projects_collection.id,
        description="A totally awesome project.")
    DBSession.add(project_one)

    # p_one_pay_rate = ManagedDecimalValue(value=29.42)

    project_one.children = [
        ManagedObjectField.create(
            name=p_pay_rate.name, parent_id=project_one.id, field_id=p_pay_rate.id,
            decimal=ManagedDecimalValue.create(value=29.42))
    ]

    # default_username = get_default_username()
    # user1 = User(id=get_id(), name=default_username, parent_id=users_type.id,
    #              fullname=default_username,
    # login=default_username)
    # DBSession.add(user1)
    #
    # user1role = UserRole(role_id=admin_role.id, user_id=user1.id)
    # DBSession.add(user1role)
    #
    # user2 = User(id=get_id(), name='Bob Dobalina', parent_id=users_type.id,
    # fullname="Robert B. Dobalina, Jr.",
    # login="bob@gmail.com")
    # DBSession.add(user2)
    #
    # user2_email = EmailAddress(user_id=user2.id, email="bob+email@gmail.com")
    # DBSession.add(user2_email)
    #
    # user2role = UserRole(role_id=user_role.id, user_id=user2.id)
    # DBSession.add(user2role)


def main(argv=sys.argv):
    """
    Initialize the database and create default records.
    :param argv: config_uri is required first element in this list.
    :return: None
    """
    if len(argv) < 2:
        usage(argv)
    config_uri = argv[1]
    options = parse_vars(argv[2:])
    setup_logging(config_uri)
    settings = get_appsettings(config_uri, options=options, name="selmy")
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)

    # Must ensure Apex models are created first because we depend on them for FKs
    from apex.models import Base as ApexBase

    ApexBase.metadata.create_all(engine)

    Base.metadata.create_all(engine)

    with transaction.manager:
        add_default_records()


if __name__ == "__main__":
    main()
