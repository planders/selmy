# !/usr/bin/env python
# -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-
# vim: fileencoding=utf-8 tabstop=4 expandtab shiftwidth=4

"""
Set up test users for the test suite.
"""

from sqlalchemy.orm.exc import NoResultFound
from apex.models import AuthUser
import transaction
from nose.tools import nottest

from selmy.tests import TEST_USERS
from selmy.models import DBSession
from selmy.util import repr_auth_user, u

__author__ = 'Preston Landers (planders@gmail.com)'
__copyright__ = 'Copyright (c) 2017 Preston Landers'
__license__ = 'BSD'


@nottest
def setup_test_users():
    print("Setting up test users.")
    from apex.lib.libapex import create_user

    with transaction.manager:
        for user_dict in TEST_USERS:
            # Don't create if already exists
            username = user_dict["username"]
            try:
                auth_user = DBSession.query(AuthUser).filter_by(login=u(username)).one()
                print("User already existed: %s => %s" % (username, repr_auth_user(auth_user)))
            except NoResultFound:
                auth_user = create_user(**user_dict)
                print("User created: %s => %s" % (username, repr_auth_user(auth_user)))
    return


@nottest
def remove_test_users():
    print("Removing test users.")
    with transaction.manager:
        for user_dict in TEST_USERS:
            # Don't create if already exists
            username = user_dict["username"]
            try:
                auth_user = DBSession.query(AuthUser).filter_by(login=u(username)).one()
                DBSession.delete(auth_user)
                print("User deleted: %s => %s" % (username, repr_auth_user(auth_user)))
            except NoResultFound:
                print("User not found to delete: %s" % (username,))
    return
