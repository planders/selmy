# !/usr/bin/env python
# -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-
# vim: fileencoding=utf-8 tabstop=4 expandtab shiftwidth=4

"""
Selmy command line console
"""

import argparse
import logging
import sys

import six

from selmy import bootstrap
from selmy.constants import SELMY_VERSION
from selmy.i18n import _, make_localizer

__author__ = 'Preston Landers (planders@gmail.com)'
__copyright__ = 'Copyright (c) 2017 Preston Landers'
__license__ = 'BSD'

description = _("""\
Selmy application server console for administrators.
""")


def main():
    """
    Process command line arguments for the selmy console
    :return: status code
    """
    # Need to get a localizer to translate the options and help
    # before we bootstrap, because we need the options parsed
    # to get the config file to bootstrap.
    localizer = make_localizer()
    tr = localizer.translate
    parser = argparse.ArgumentParser(
        prog="selmy",
        description=tr(description)
    )
    parser.add_argument(
        '--version', '-v',
        version='%(prog)s ' + SELMY_VERSION,
        action='version'
    )
    parser.add_argument(
        '--config', '-c',
        dest='config',
        metavar='URI',
        type=str,
        action='store',
        help=tr(_("Specify the config_uri for the app. Overrides SELMY_CONFIG env var."))
    )
    parser.add_argument(
        'command',
        nargs='?',
        default='shell',
        help=tr(_('Command to run (default: shell)')),
    )
    parser.add_argument(
        'command_arguments',
        nargs=argparse.REMAINDER,
    )

    options = parser.parse_args(sys.argv[1:])
    config_uri = options.config
    command = options.command
    command_arguments = options.command_arguments
    env = bootstrap(config_uri)
    log = logging.getLogger(__name__)

    # This is a terrible idea here and will break, but I'm removing the console logger
    log.root.removeHandler(log.root.handlers[0])

    console = Console(env, log, command, command_arguments)
    return console() or 0


# noinspection PyMethodMayBeStatic
class Console(object):
    """
    Run selmy console commands, and/or interactive console.

    Usage:

        result = Console("command", ['arg1', 'arg2'])()
    """

    HELP_STR = _("""\
    Available commands:
        * quit
        * help
        * show
        * setup_test_users
        * remove_test_users
    """)

    def __init__(self, env, log, command='shell', arguments=None):
        """
        Run selmy console commands including 'shell' for the interactive console.

        :param env: pyramid environment
        :param log: logger object
        :param command: the command to run. Default: 'shell'
        :param arguments: additional arguments list for the command
        :return: error code (0 for success)
        """
        self.env = env
        self.tr = env['request'].localizer.translate
        self.log = log
        self.command = command
        self.arguments = arguments or []

    def __call__(self, **kwargs):
        if 'command' in kwargs:
            self.command = kwargs['command']
        if 'arguments' in kwargs:
            self.arguments = kwargs['arguments']
        return self.run()

    def run(self):
        """
        Run the current command.
        """
        rc = 0
        tr = self.env['request'].localizer.translate
        if self.command != 'shell':
            return self.interpret_command()

        print(tr(_("Welcome to Selmy ${ver}!", mapping={'ver': SELMY_VERSION})))
        print(tr(_("Press Ctrl-C to quit.")))
        try:
            from prompt_toolkit import prompt
            from prompt_toolkit.history import InMemoryHistory
            history = InMemoryHistory()
        except ImportError:
            self.log.warning("Unable to import prompt_toolkit module. Try `pip install iPython`")
            prompt = history = None

        prompt_text = tr(_("Selmy> "))
        while True:
            # TRANSLATORS: command line prompt
            try:
                if prompt:
                    try:
                        commandline = prompt(prompt_text, history=history)
                    except:
                        commandline = six.moves.input(prompt_text)
                else:
                    commandline = six.moves.input(prompt_text)
                if not commandline:
                    continue
            except (KeyboardInterrupt, EOFError):
                break
            if commandline.lower() == "quit":
                break
            command = commandline.strip().split(" ", 1)
            self.command = command[0]
            self.arguments = command[1:]
            rc = self.interpret_command()

        return rc

    def interpret_command(self):
        """
        This is where we interpret the current command.
        :return:
        """
        tr = self.tr
        rc = 0
        if self.command == "show":
            rc = self.cmd_show()
        elif self.command == "help":
            self.cmd_help()
        elif self.command == "bootuser":
            self.boot_user()
        elif self.command == "setup_test_users":
            self.setup_test_users()
        elif self.command == "remove_test_users":
            self.remove_test_users()
        else:
            print(tr(_("Unknown command.")) + "\n")
            return 1
        return rc

    def cmd_help(self):
        print(self.tr(self.HELP_STR))
        if 'show' in self.arguments:
            print(self.tr(self.SHOW_HELP))

    SHOW_HELP = _("The show command reports information about the named object.")

    def cmd_show(self):
        """
        The 'show' command will report lots of information.

        :return:
        """
        # print("SHOW: %s %s" % (self.command, self.arguments))
        if len(self.arguments) < 1:
            self.cmd_help()
            return 1
        show_thing = self.arguments[0]
        if show_thing == 'url':
            # TODO: this isn't showing the port from the .ini file!
            app_url = self.env['request'].application_url
            print(app_url)
        else:
            show_args = self.arguments[1:]
            self.show_object(show_thing, show_args)
        return 0

    def show_object(self, objType, showArgs):
        if objType == 'user':
            from selmy.models import DBSession
            from selmy.models.user import User

            objList = list(DBSession.query(User))
            for obj in objList:
                print(obj)
        else:
            print(self.tr(_("Unknown object.")))

    def boot_user(self):
        user_name = input("User Display Name> ")
        user_login = input("User Login > ")
        user_fullname = input("User Full Name > ")
        print(
            "Creating username %(user_name)s (%(user_fullname)s) <Login: %(user_login)s>" % locals())

    def setup_test_users(self):
        from selmy.scripts.setup_test_users import setup_test_users

        setup_test_users()

    def remove_test_users(self):
        from selmy.scripts.setup_test_users import remove_test_users

        remove_test_users()


if __name__ == "__main__":
    sys.exit(main())
