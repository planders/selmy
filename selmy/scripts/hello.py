# !/usr/bin/env python
# -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-
# vim: fileencoding=utf-8 tabstop=4 expandtab shiftwidth=4

"""
A utility / testing script to create some demo records.
"""

import sys
from decimal import Decimal
from logging import getLogger

import transaction
from pyramid.paster import setup_logging

from selmy import bootstrap
from selmy.i18n import _
from selmy.models import DBSession
from selmy.models.managed import RootType, ManagedType, ManagedField, ManagedObject, \
    ManagedObjectField
from selmy.models.managed.managed_field_data_type import \
    ManagedFieldDataType, ManagedDecimalValue, ManagedUnicodeValue, ManagedBooleanValue
from selmy.util import u

__author__ = 'Preston Landers (planders@gmail.com)'
__copyright__ = 'Copyright (c) 2017 Preston Landers'
__license__ = 'BSD'


def some_fake_records():
    root_type = RootType.get_root_instance()

    widget = ManagedType.create(
        name="Widget", description="Generic widgets.",
        parent_id=None)
    DBSession.add(widget)
    DBSession.flush()

    widget.ensure_collections()  # TODO

    widgets_collection = root_type[u('Widget')]

    text_type = ManagedFieldDataType.get_type_by_capability(ManagedFieldDataType.UNICODE)
    decimal_type = ManagedFieldDataType.get_type_by_capability(ManagedFieldDataType.DECIMAL)
    bool_type = ManagedFieldDataType.get_type_by_capability(ManagedFieldDataType.BOOLEAN)

    w_active = ManagedField.create(
        order=3, name="Active", description="Is widget active?", parent_id=widget.id,
        data_type_id=bool_type.id)
    w_flux_rate = ManagedField.create(
        order=2, name="Flux Rate", description="Flux capacitance rate", parent_id=widget.id,
        data_type_id=decimal_type.id)
    w_note = ManagedField.create(
        order=1, name="Note", description="Note field.", parent_id=widget.id,
        data_type_id=text_type.id)

    widget_fields = [w_note, w_flux_rate, w_active]
    DBSession.add_all(widget_fields)
    DBSession.flush()

    for i in range(1, 101):
        # Now add a project and some field values.
        a_widget = ManagedObject.create(
            name="Generic Widget #%s" % (i,),
            parent_id=widgets_collection.id,
            description="Automatically generated widget %s" % (i,))
        DBSession.add(a_widget)

        a_widget.children = [
            ManagedObjectField.create(
                name=w_flux_rate.name, parent_id=a_widget.id, field_id=w_flux_rate.id,
                field=w_flux_rate,
                decimal=ManagedDecimalValue.create(value=Decimal("29.42") * i)),
            ManagedObjectField.create(
                name=w_note.name, parent_id=a_widget.id, field_id=w_note.id,
                field=w_note,
                unicode=ManagedUnicodeValue.create(value="Note for %s" % (i,))),
            ManagedObjectField.create(
                name=w_active.name, parent_id=a_widget.id, field_id=w_active.id,
                field=w_active,
                boolean=ManagedBooleanValue.create(value=True))

        ]

    return


def cleanup_records():
    managed_type = RootType.get_root_instance()['System']['Type']['Managed']
    try:
        widget_type = managed_type['Widget']
    except KeyError:
        return
    DBSession.delete(widget_type)
    DBSession.flush()
    return


def cleanup_records2():
    widget_type = RootType.get_root_instance()['System']['Type']['Managed']['Widget']
    try:
        active_field = widget_type['Active']
    except KeyError:
        return
    DBSession.delete(active_field)
    DBSession.flush()
    return


def main(argv=sys.argv):
    if len(argv) > 1:
        ini_file = argv[1]
    else:
        ini_file = "local.ini"
    env = bootstrap(config_uri=ini_file)  # must call before getLogger for it to work
    setup_logging(ini_file)
    log = getLogger(__file__)

    # Get a translated string
    hw = env['request'].localizer.translate(_("Hello!"))

    # Check logging
    log.warning("hello.py greeting: %s", hw)

    # Do something
    print("Greeting: %s" % hw)

    with transaction.manager:

        cleanup_records()
        # cleanup_records2()

        some_fake_records()


if __name__ == "__main__":
    main()
