# !/usr/bin/env python
# -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-
# vim: fileencoding=utf-8 tabstop=4 expandtab shiftwidth=4

"""
Selmy Domain Model - metadata
"""

import datetime
from collections import OrderedDict
from logging import getLogger

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.schema import MetaData

from selmy.constants import API
from selmy.util import strip_trailing_slash

__author__ = 'Preston Landers (planders@gmail.com)'
__copyright__ = 'Copyright (c) 2017 Preston Landers'
__license__ = 'BSD'

log = getLogger(__name__)

# : Establish a naming convention for constraints and other objects for consistency
# : http://docs.sqlalchemy.org/en/rel_0_9/core/constraints.html#constraint-naming-conventions
convention = {
    "ix": 'ix_%(column_0_label)s',
    "uq": "uq_%(table_name)s_%(column_0_name)s",
    "ck": "ck_%(table_name)s_%(constraint_name)s",
    "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
    "pk": "pk_%(table_name)s"
}


class SelmyModelBase(object):
    """
    Base class for Selmy data model classes.
    """

    ATTR_URI = "uri"

    def __json__(self, request, depth=1) -> OrderedDict:
        """
        Get a JSON representation of this resource.
        :param request: the pyramid request
        :type request: Request
        :param depth: Number of recursive levels of JSON objects to allow.
        If zero, we return a summary representation.
        :type depth: int
        :return: dictionary ready for JSON encoding
        """
        if not depth:
            to_serialize = self.get_serialize_summary_keys()
        else:
            to_serialize = self.get_serialize_keys()
        d = OrderedDict()
        for attr_name in to_serialize:
            real_name = display_name = attr_name
            if isinstance(attr_name, tuple):
                display_name = attr_name[0]
                real_name = attr_name[1]

            if display_name == self.ATTR_URI:
                d[display_name] = self.__uri__(request, absolute=True, prefix=API)
            else:
                val = getattr(self, real_name, None)
                if isinstance(val, datetime.datetime):
                    # TODO: I think this is handled by the general JSON renderer...
                    # TODO: timezone aware datetime?
                    val = val.isoformat()
                elif callable(val):
                    val = val(request)
                if isinstance(val, dict):
                    pass
                elif hasattr(val, "__json__"):
                    # val = val.__json__(request, depth=(depth-1))
                    if hasattr(val, "parent_id"):
                        val = val.__json__(request, depth=(depth - 1))
                    else:
                        val = val.__json__(request, depth=depth)
                elif isinstance(val, str):
                    pass
                elif hasattr(val, "__iter__"):
                    # val = [str(x) for x in val]
                    new_val = []
                    for sub_val in val:
                        if hasattr(sub_val, "__json__"):
                            # If this is in a normal hierarchy, only show a summary. otherwise
                            # it's assumed to be a secondary object that should be given in full.
                            if hasattr(sub_val, "parent_id"):
                                new_val.append(sub_val.__json__(request, depth=(depth - 1)))
                            else:
                                new_val.append(sub_val.__json__(request, depth=depth))
                        elif isinstance(sub_val, dict):
                            new_val.append(sub_val)
                        else:
                            new_val.append(str(sub_val))
                    val = new_val
                d[display_name] = val
        return d

    @classmethod
    def from_json(cls, data):
        return cls(**data)

    def get_serialize_keys(self):
        return []

    @staticmethod
    def get_serialize_summary_keys():
        """
        Return the attributes to include in the summary JSON description.
        :return: list of attributes
        :rtype: list
        """
        return ['name', 'id', 'resource_type', 'uri', 'parent_id', 'parent_uri']

    # noinspection PyUnusedLocal
    def resource_type(self, request):
        # Is there a builtin way to do this?
        table_name = getattr(self, "__tablename__", None)
        if table_name:
            return table_name
        return self.__class__.__name__

    def parent_uri(self, request, absolute=True, prefix=API):
        parent = getattr(self, "__parent__", None)
        if not parent:
            return None
        return parent.__uri__(request, absolute=absolute, prefix=prefix)

    def __uri__(self, request, absolute=False, prefix=None, trailing_slash=False):
        """
        Get a URI link to this resource.

        :param request: the pyramid request
        :type request: pyramid.request.Request
        :param absolute: if True, return an absolute URL with protocol etc
        :type absolute: bool
        :param prefix: A prefix to insert before the resource, e.g. "edit"
        :type prefix: str
        :param trailing_slash: if True allow the trailing slash to occur
        :type trailing_slash: bool
        :return: link to this resource
        :rtype: str
        """
        res_path = request.resource_path(self)
        if prefix:
            res_path = "/" + prefix + res_path
        # noinspection PyUnresolvedReferences
        app_prefix = request.registry.settings.get("app_prefix", "")
        res_path = app_prefix + res_path
        if absolute:
            res_path = request.application_url + res_path
        if not trailing_slash:
            res_path = strip_trailing_slash(res_path)
        return res_path

    def update_from_json(self, patch_json):
        """
        Update value instances from a possibly incomplete JSON object,
        assumed to be user input from a POST/PATCH type command.

        TODO: VALIDATION IS NEEDED!

        Don't allow changing:
            * Create time
            * Modify time
            * Parent - except under limited circumstances?
            * A name to an already existing name
            * URI should be explicitly read-only
            * Field must be defined?!
        """
        # TODO: Validate JSON here?!
        to_serialize = self.get_serialize_keys()
        for patch_key in patch_json:
            # TODO Validate that we're allowed to set this particular key!!
            if patch_key not in to_serialize:
                raise ValueError("This object does not have a field %r" % (patch_key,))
            decoded_val = self.deserialize_attribute(patch_key, patch_json[patch_key])
            if patch_key != "contents":
                # This is a hack - I need to figure out what to do here.
                # 'contents' is handled above.
                setattr(self, patch_key, decoded_val)

        return self

    # noinspection PyUnusedLocal,PyMethodMayBeStatic
    def deserialize_attribute(self, attrib_name, attrib_value):
        """
        Convert an incoming user expression of a value and convert it to a native type
        as appropriate. For instance, if attrib_name matches a Decimal typed field,
        convert attrib_value to a Decimal instance.  Default implementation does nothing.
        """
        return attrib_value

    @classmethod
    def create(cls, *args, **kwargs):
        """
        Standard (class) factory method for creating new instances of this type. Does NOT
        add it to the database session automatically.
        """
        log.debug("Calling %s.create(%s, %s)", cls.__name__, args, kwargs)
        return cls(*args, **kwargs)

    @staticmethod
    def ensure_collections():
        """
        If this is a Type Definition, ensure the collection objects exist for all relevant objects.
        """
        return


metadata = MetaData(naming_convention=convention)

# : Base class for *all* Selmy data tables - not just the main domain types (see Node)
Base = declarative_base(cls=SelmyModelBase, metadata=metadata)
