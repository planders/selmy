# !/usr/bin/env python
# -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-
# vim: fileencoding=utf-8 tabstop=4 expandtab shiftwidth=4

"""
Selmy User object.
"""

from logging import getLogger

from apex.models import AuthUser
from sqlalchemy import Column, ForeignKey, Unicode, UnicodeText, Boolean, Integer
from sqlalchemy.orm import relationship, backref

from selmy.models import (
    Base, BigIntegerType, STD_NAME_LEN,
    rel_cascade, CASCADE, SET_NULL)
from selmy.models.system import SystemType

__author__ = 'Preston Landers (planders@gmail.com)'
__copyright__ = 'Copyright (c) 2017 Preston Landers'
__license__ = 'BSD'

log = getLogger(__name__)


class User(SystemType):
    """
    This is the main user profile object.

    There is also a related Authentication User object, which controls
    the password, OpenID identity, etc.

    parent_id should be (TODO)
    """
    __tablename__ = 'user'
    system_type_name = "User"  # : Name of the type as presented to end users
    id = Column(BigIntegerType, ForeignKey(
        'system_type.id', onupdate=CASCADE, ondelete=CASCADE), primary_key=True)
    auth_id = Column(Integer, ForeignKey(
        AuthUser.id, onupdate=CASCADE, ondelete=SET_NULL), index=True, nullable=True)

    """
    In your templates, you can access the user object through the
    request context as :

    **request.user** or **request.user.profile.fullname**
    """

    # : the user's full name - split into two or more fields?
    fullname = Column(Unicode(STD_NAME_LEN))
    enabled = Column(Boolean(name='enabled'))  # doesn't do anything yet

    # TODO: additional profile info here.

    # Additional properties and relationships:

    # : the Authentication User record - TODO: which way should the cascade flow?!
    auth_user = relationship(
        AuthUser, backref=backref('profile', uselist=False, cascade=rel_cascade))

    roles = relationship("UserRole", backref=backref(__tablename__, cascade=rel_cascade))

    system_type_description = "User profiles."

    def get_serialize_keys(self):
        return super(User, self).get_serialize_keys() \
               + ['fullname', 'enabled', 'email_address', 'roles']


class EmailAddress(Base):
    """
    TODO: enforce email uniqueness? I.e, don't allow more than one user to have a given address?
    """
    __tablename__ = "email_address"
    # id = Column(BigIntegerType, primary_key=True)
    user_id = Column(
        BigIntegerType, ForeignKey('user.id', onupdate=CASCADE, ondelete=CASCADE), primary_key=True)
    email = Column(Unicode(length=STD_NAME_LEN), primary_key=True, nullable=False)
    verified = Column(Boolean(name='verified'), nullable=False, default=False)

    user = relationship(
        "User", backref=backref(__tablename__, order_by=email, cascade=rel_cascade))

    def get_serialize_keys(self):
        return ['email', 'verified']

    @property
    def __parent__(self):
        return self.user

    @property
    def __name__(self):
        return self.email

    def get_serialize_summary_keys(self):
        """
        Return the attributes to include in the summary JSON description.
        :return: list of attributes
        :rtype: list
        """
        return self.get_serialize_keys()


class Role(SystemType):
    __tablename__ = "role"
    system_type_name = "Role"  # : Name of the type as presented to end users
    id = Column(
        BigIntegerType,
        ForeignKey('system_type.id', onupdate=CASCADE, ondelete=CASCADE), primary_key=True)
    description = Column(UnicodeText)

    system_type_description = "System authorization roles."

    def get_serialize_keys(self):
        return super(Role, self).get_serialize_keys() + ['abilities']

    # def __repr__(self):
    # return "<Role(name='%s')>" % (self.name,)

    @staticmethod
    def add_default_records(system_collection):
        from .. import DBSession

        admin_role = Role(name='admin', parent_id=system_collection.id, description="Administrator")
        DBSession.add(admin_role)

        user_role = Role(name='user', parent_id=system_collection.id, description="Regular User")
        DBSession.add(user_role)
        DBSession.flush()

        abil1 = RoleAbility(role_id=admin_role.id, ability='edit', value='*')
        DBSession.add(abil1)
        abil2 = RoleAbility(role_id=admin_role.id, ability='view', value='*')
        DBSession.add(abil2)


class UserRole(Base):
    __tablename__ = "user_role"
    user_id = Column(
        BigIntegerType,
        ForeignKey('user.id', onupdate=CASCADE, ondelete=CASCADE), primary_key=True)
    # role_id = Column(
    #     BigIntegerType, ForeignKey('roles.id', onupdate=CASCADE, ondelete=CASCADE),
    #     primary_key=True)

    # Manually deleted roles must clean these up...
    role_id = Column(BigIntegerType, ForeignKey('role.id'), primary_key=True)

    # user = relationship("User", backref=backref(__tablename__, cascade=CASCADE))
    # role = relationship("Role", backref=backref(__tablename__, cascade=CASCADE))
    #
    # get_serialize_keys = ['role']

    role = relationship("Role", backref=backref("role_users"), cascade=rel_cascade)


class RoleAbility(Base):
    __tablename__ = "role_ability"
    role_id = Column(
        BigIntegerType,
        ForeignKey('role.id', onupdate=CASCADE, ondelete=CASCADE), primary_key=True)
    ability = Column(Unicode(2000), primary_key=True, nullable=False)
    value = Column(UnicodeText, nullable=False)

    role = relationship("Role", backref=backref("abilities"), cascade=rel_cascade)

    def get_serialize_keys(self):
        return ['ability', 'value']

    # def __repr__(self):
    #     return "<RoleAbility(role='%s', ability='%s', value='%s')>" % (
    #         self.role.get_resource_path(), self.ability, self.value)
