# !/usr/bin/env python
# -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-
# vim: fileencoding=utf-8 tabstop=4 expandtab shiftwidth=4

"""
Selmy Domain Model
"""

import itertools
from datetime import datetime
from logging import getLogger

import six
from pyramid.security import ALL_PERMISSIONS, Allow, Authenticated, DENY_ALL
from pyramid.traversal import resource_path
from pytz import UTC
from six.moves.urllib_parse import unquote_plus
from sqlalchemy import BigInteger, DateTime
from sqlalchemy.dialects import mysql, postgresql, sqlite
from sqlalchemy.exc import InvalidRequestError
from sqlalchemy.orm import Session, reconstructor, scoped_session, sessionmaker
from sqlalchemy.orm.exc import NoResultFound, DetachedInstanceError
from sqlalchemy.types import TypeDecorator
from zope.sqlalchemy import ZopeTransactionExtension

from selmy.exceptions import RequiredValueError
from selmy.models.meta import Base
from selmy.util import repr_path, u, utc_now

__author__ = 'Preston Landers (planders@gmail.com)'
__copyright__ = 'Copyright (c) 2017 Preston Landers'
__license__ = 'BSD'

log = getLogger(__name__)
DBSession: Session = scoped_session(sessionmaker(extension=ZopeTransactionExtension()))

# : the default cascade option to use with foreign keys (not automatic!)
rel_cascade = "all"
rel_delete_orphan = "all, delete-orphan"

# : You can use these in onupdate, ondelete options on ForeignKey
CASCADE = "CASCADE"
SET_NULL = "SET NULL"

# : The name of the special System resource node
SYSTEM = "System"
TYPE_ROOT = "Type"
MANAGED_TYPE_ROOT = "Managed"

# SQLAlchemy does not map BigInt to Int by default on the sqlite dialect.
# It should, but it doesnt.
# http://stackoverflow.com/a/23175518/858289
BigIntegerType = BigInteger()
BigIntegerType = BigIntegerType.with_variant(postgresql.BIGINT(), 'postgresql')
BigIntegerType = BigIntegerType.with_variant(mysql.BIGINT(), 'mysql')
BigIntegerType = BigIntegerType.with_variant(sqlite.INTEGER(), 'sqlite')

# : standard maximum length for names that should be limited
STD_NAME_LEN = 2000


def in_name_or_collection(a_key, a_collection):
    if a_key in a_collection:
        return True
    return a_key in [col_elem.name for col_elem in a_collection]


def get_id_maker(start_at=1):
    """
    Gets a function to generate new integer IDs for database default record creation purposes.
    (Not guaranteed to be unique in any sense.)

    :param start_at: number to start counting at, defaults to 1
    :type start_at: int
    :return: function to call to make IDs.
    :rtype: function
    """

    def get_id():
        get_id.counter += 1
        return get_id.counter

    get_id.counter = start_at - 1
    return get_id


class UTCDateTime(TypeDecorator):
    """
    Use this as your primary :py:class`DateTime` type. It wraps the standard DateTime
    to force UTC awareness.  All DateTimes are stored with UTC zone and
    should be converted to local zone for display.

    This type decorator will raise a ValueError if you attempt to use a
    'naive' datetime - one without a timezone.
    """
    impl = DateTime

    def __init__(self, *args, **kwargs):
        kwargs['timezone'] = True
        super(UTCDateTime, self).__init__(*args, **kwargs)
        # log.debug("init of UTCDateTime: %s %s", args, kwargs)

    def process_bind_param(self, value, engine):
        if value is not None:
            return value.astimezone(UTC)

    def process_result_value(self, value, engine):
        if value is not None:
            return datetime(
                value.year, value.month, value.day,
                value.hour, value.minute, value.second,
                value.microsecond, tzinfo=UTC)


class NodeMixin(object):
    """
    A mixin class to implement Node behaviors.

    The base data type for structured domain types in Selmy.

    All 'main' data types should inherit from ManagedNode, ManagedType,
    or SystemTypeDefinition to be reachable through standard traversal and security.

    It is also possible to have subsidiary or attribute tables that are related
    to a Node-derived table (through a FK) but do not inherit from it.

    TODO: support __delitem__ ??

    """

    # noinspection PyUnusedLocal
    def __init__(self, *args, **kwargs):
        # These are mainly for PyCharm so it doesn't think these are made up
        self.id = None
        self.parent = None
        self.parent_id = None
        self.name = None
        self.children = []
        super(NodeMixin, self).__init__()

        now = utc_now()
        if not self.create_time:
            self.create_time = now
        if not self.modify_time:
            self.modify_time = now

    def __setitem__(self, key, node):
        node.name = u(key)
        if self.id is None:
            DBSession.flush()
        node.parent_id = self.id
        DBSession.add(node)
        DBSession.flush()

    def __getitem__(self, key):
        try:
            return DBSession.query(self.__class__).filter_by(
                name=u(key), parent=self).one()
        except NoResultFound:
            raise KeyError(key)

    def __iter__(self):
        return iter(self.values())

    def values(self):
        return iter(self.children)

    # noinspection PyUnusedLocal
    def contents(self, request):
        return iter(self.children)

    def __len__(self):
        return len(self.children)

    def __contains__(self, item):
        return in_name_or_collection(item, self.children)

    @property
    def __name__(self):
        try:
            return self.name
        except (InvalidRequestError, DetachedInstanceError):
            # most likely tried to access the property while in an invalid state
            # incl. during debugging
            return None

    # : VirtualNodes will set this to properly place items into the master resource tree
    _resource_parent = None

    @property
    def __parent__(self):
        if self._resource_parent is not None:
            return self._resource_parent
        try:
            return self.parent
        except InvalidRequestError:
            return None  # See comment in __name__

    def get_serialize_keys(self):
        return [
            'name',
            'id',
            'resource_type',
            'uri',
            'parent_id',
            'parent_uri',
            'create_time',
            'modify_time',
            'description',
            'contents',
        ]

    @property
    def __acl__(self):
        """
        Retrieve the Access Control List of this node.

        TODO: this should do something cool.
        Currently it allows authenticated users full access.
        """
        return [
            (Allow, Authenticated, ALL_PERMISSIONS),
            DENY_ALL
        ]

    def get_path_name(self, unquote=False):
        """
        Return the complete path name of this resource.

        TODO: is there a builtin way to do this?
        """
        res = u(resource_path(self))
        if unquote:
            res = unquote_plus(res)
        return res

    def __repr__(self):
        rpath = repr_path(self)
        return u"<Resource %s>" % (rpath,)

    def deserialize_attribute(self, attrib_name, attrib_value):
        """
        Convert an incoming user expression of a value and convert it to a native type
        as appropriate. For instance, if attrib_name matches a Decimal typed field,
        convert attrib_value to a Decimal instance.  Default implementation does nothing.
        """
        if attrib_name != "contents":
            return attrib_value
        for content_item in attrib_value:
            try:
                content_name = content_item["name"]
            except KeyError:
                # TODO: more formalized way of indicating JSON schema errors!
                # Translation string
                raise RequiredValueError(
                    "Every item in 'contents' must supply a 'name' key and optionally a "
                    "'resource_type' key. Was: %s" % (content_item,))
            if content_name not in self:
                self.create_new_child_item_from_json(content_item)
                # Don't need to store reference to model_item here since
                # it should already be placed in the hierarchy
            else:
                model_item = self[content_name]
                model_item.update_from_json(content_item)

        return attrib_value

    def create_new_child_item_from_json(self, content_json):
        # Previously verified this key was present
        content_name = content_json["name"]

        new_child = self.get_new_child_instance(self.get_child_class_for_name(content_name))
        self.update_new_child_basic_values(new_child, content_name)
        new_child.update_from_json(content_json)

        # Re-force key items... want a better way to do this
        # self.update_new_child_basic_values(new_child, content_name)
        return new_child

    @staticmethod
    def get_new_child_instance(child_class, *args, **kwargs):
        new_child = child_class.create(*args, **kwargs)
        return new_child

    def update_new_child_basic_values(self, new_child, new_name):
        new_child.name = new_name
        new_child.parent = self
        new_child.parent_id = self.id
        return new_child

    def get_child_class_for_name(self, content_name):
        raise NotImplementedError("Subclasses must implement get_child_class_for_name")


class HybridNodeMixin(object):
    """
    A HybridNodeMixin is added to a basic ManagedNode to keep a set of
    special keys which are handled as regular Python objects instead of database recs.
    These additional keys are not persisted in any way so they should be set by the
    constructing code.
    """

    @property
    def __special_names__(self):
        return []

    # noinspection PyUnusedLocal
    def __init__(self, *args, **kwargs):
        super(HybridNodeMixin, self).__init__()

        self._setup_data()

    @reconstructor
    def _setup_data(self):
        # : A field to keep track of special non-database resources.
        self.data = {}

    def __getitem__(self, key):
        key = u(key)
        if key in self.__special_names__:
            return self.data[key]
        raise KeyError(key)

    def __setitem__(self, key, value):
        if key in self.__special_names__:
            self.data[key] = value
        else:
            raise ValueError("Not a valid key for this node: %s" % (key,))

    def __iter__(self):
        children = getattr(self, "children", [])
        return itertools.chain(six.itervalues(self.data), children)

    def __len__(self):
        children = getattr(self, "children", [])
        return len(children) + len(self.data)

    def __contains__(self, item):
        if in_name_or_collection(item, self.data):
            return True
        children = getattr(self, "children", [])
        return in_name_or_collection(item, children)

    @staticmethod
    def get_serialize_keys():
        return [
            'name', 'id', 'resource_type', 'uri', 'parent_id', 'parent_uri', 'description',
            ('contents', 'children_plus_nodes')
        ]

    def children_plus_nodes(self, request):
        res = []
        for node_key, node_value in self.data.items():
            res.append(node_value.__json__(request, depth=0))
        children = getattr(self, "children", [])
        return itertools.chain(res, children)


# noinspection PyUnusedLocal
def root_factory(request):
    """
    Returns the Pyramid root object factory for traversal.
    :param request: the Pyramid request
    :return: the RootType instance that is our site root
    """
    from .managed import RootType

    # noinspection PyBroadException
    try:
        root = RootType.get_root_instance()
    except:
        log.error("Can't get root object.", exc_info=True)
        return None
    return root


# noinspection PyUnusedLocal
def before_update__modify_time(mapper, connection, target):
    """
    Listen for SQL UPDATE events and populate the modify_time field.

    :param mapper: SQLAlchemy mapper
    :param connection: SQLAlchemy connection object
    :param target: record being modified
    :return: None
    """
    target.modify_time = utc_now()
    return


# noinspection PyProtectedMember
def get_class_for_table_name(table_name):
    """
    Return the class associated with a given string table name.

    :param table_name: a table name
    :type table_name: str
    :return: the associated class
    :rtype: Class
    """
    try:
        the_class = Base._decl_class_registry[table_name]
    except KeyError:
        the_class = None
    if not the_class:
        for meta_name, meta_class in Base._decl_class_registry.items():
            if getattr(meta_class, "__tablename__", None) == table_name:
                the_class = meta_class
                break
    return the_class
