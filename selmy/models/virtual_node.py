#!/usr/bin/env python
# -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-
# vim: fileencoding=utf-8 tabstop=4 expandtab shiftwidth=4

"""
Resource nodes which aren't primarily database based.
"""

import itertools
from logging import getLogger

import six
from pyramid.traversal import resource_path
from six.moves.urllib_parse import unquote_plus
from sqlalchemy.orm.exc import NoResultFound

# noinspection PyProtectedMember
from selmy.i18n import _
from selmy.models import DBSession, MANAGED_TYPE_ROOT, SYSTEM, TYPE_ROOT, in_name_or_collection
from selmy.models.managed import ManagedType
from selmy.models.meta import SelmyModelBase
from selmy.models.system import SystemCollection, SystemTypeDefinition
from selmy.util import repr_path, u

__author__ = 'Preston Landers (planders@gmail.com)'
__copyright__ = 'Copyright (c) 2017 Preston Landers'
__license__ = 'BSD'

log = getLogger(__name__)


class VirtualNode(SelmyModelBase):
    """
    A node in the resource tree that doesn't have a direct representation in
    the database.
    """

    def __init__(self, name, parent):
        self.name = name
        self.parent = parent
        self.data = {}
        super(VirtualNode, self).__init__()

    def __setitem__(self, key, node):
        node.name = u(key)
        node.parent = self
        self.data[key] = node

    def __getitem__(self, key):
        return self.data[u(key)]

    @property
    def __name__(self):
        return self.name

    @property
    def __parent__(self):
        return self.parent

    def get_path_name(self, unquote=False):
        """
        Return the complete path name of this resource as Unicode.
        """
        res = u(resource_path(self))
        if unquote:
            res = unquote_plus(res)
        return res

    def __repr__(self):
        rpath = repr_path(self)
        return u"<Resource %s>" % (rpath,)

    def __iter__(self):
        return six.itervalues(self.data)

    def __len__(self):
        return len(self.data)

    def __contains__(self, item):
        return in_name_or_collection(item, self.data)


class NamedVirtualNode(VirtualNode):
    """
    A node base class that has a fixed name given as the self.NAME property
    and that manages a subclass given as the self.SUBCLASS property. The
    subclass is a collection of some kind.
    """
    # noinspection PyPep8Naming
    @property
    def NAME(self):
        raise NotImplementedError("Subclasses must set the NAME property.")

    # noinspection PyPep8Naming
    @property
    def SUBCLASS(self):
        raise NotImplementedError("Subclasses must set the SUBCLASS property.")

    def __init__(self, parent):
        super(NamedVirtualNode, self).__init__(self.NAME, parent)

    def __getitem__(self, key):
        key = u(key)
        if key in self.data:
            return self.data[key]
        # Do a collection lookup for the SUBCLASS we manage
        # (parent_id is NULL)
        try:
            system_collection = DBSession.query(self.SUBCLASS).filter_by(
                name=key, parent_id=None).one()
        except NoResultFound:
            raise KeyError(key)
        return system_collection

    @property
    def children(self):
        return DBSession.query(self.SUBCLASS).filter_by(parent_id=None)

    def __iter__(self):
        return itertools.chain(six.itervalues(self.data), self.children)

    def __len__(self):
        return self.children.count() + len(self.data)

    def __contains__(self, item):
        if in_name_or_collection(item, self.data):
            return True
        return in_name_or_collection(item, self.children)

    def get_serialize_keys(self):
        return ['name', 'id', 'resource_type', 'uri', 'parent_id', 'parent_uri', 'contents']

    def contents(self, request):
        res = []
        for node_key, node_value in self.data.items():
            res.append(node_value.__json__(request, depth=0))
        return res + list(self.children)


class SystemRoot(NamedVirtualNode):
    """
    Represents /System under a RootType resource.
    Knows about two things:
        'Type'               (system and managed type definitions)
        <System Type name>   (collections of system types)
    """
    NAME = SYSTEM
    SUBCLASS = SystemCollection
    description = _("System objects and collections.")
    parent_id = 1  # Not sure if best to hardcode this


class TypeRoot(NamedVirtualNode):
    """
    Represents /System/Type under a SystemRoot resource.
    Knows about two things:
        'Managed'           (managed [user-defined] type definitions)
        <System Type Name>  (system type definition)
    """
    NAME = TYPE_ROOT
    SUBCLASS = SystemTypeDefinition
    description = _("System type definitions.")


class ManagedTypeRoot(NamedVirtualNode):
    """
    Represents /System/Type/Managed under a TypeRoot resource.
    Keys under this are ManagedTypeNode
    """
    NAME = MANAGED_TYPE_ROOT
    SUBCLASS = ManagedType
    description = _("Managed (administrator defined) type definitions.")
