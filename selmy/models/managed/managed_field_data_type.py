# !/usr/bin/env python
# -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-
# vim: fileencoding=utf-8 tabstop=4 expandtab shiftwidth=4

"""
This defines the basic data types allowed to managed fields.

TODO: add these:
* Time Interval
* Time Zone  (for user profile pref?)
"""

from decimal import Decimal

import dateutil.parser
from sqlalchemy import Column, Unicode, ForeignKey, Boolean, Float, Numeric, Date, UnicodeText, \
    LargeBinary
from sqlalchemy.orm import relationship, backref

from selmy.models import Base, BigIntegerType, STD_NAME_LEN, CASCADE, rel_cascade, UTCDateTime, \
    DBSession
from selmy.models.system import SystemType
from selmy.util import u

__author__ = 'Preston Landers (planders@gmail.com)'
__copyright__ = 'Copyright (c) 2017 Preston Landers'
__license__ = 'BSD'

# This is also the name of the backreferences.
UNICODE = 'unicode'
BOOLEAN = 'boolean'
INTEGER = 'integer'
FLOAT = 'float'
DECIMAL = 'decimal'
DATE = 'date'
DATETIME = 'datetime'
INTERVAL = 'interval'
TIMEZONE = 'timezone'
FILE = 'file'
SELECTION = 'selection'
HTML = 'html'
JSON = 'json'
XML = 'xml'

NUMERIC_CAPABILITIES = (
    INTEGER,
    FLOAT,
    DECIMAL
)

# : Number of digits that can be stored to the left of the decimal in the DECIMAL type.
DEC_PRECISION = 28

# : Number of digits that can be stored to right of the decimal in the DECIMAL type.
DEC_SCALE = 10


class ManagedFieldDataType(SystemType):
    """
    parent is ManagedDataTypesRoot
    """
    __tablename__ = "managed_field_data_type"
    system_type_name = "DataType"  # : Name of the type as presented to end users
    id = Column(
        BigIntegerType,
        ForeignKey('system_type.id', onupdate=CASCADE, ondelete=CASCADE), primary_key=True)
    description = Column(UnicodeText)
    capability = Column(Unicode, nullable=False)  # : special capability code,s comma-separated
    enabled = Column(
        Boolean(name="enabled"), default=True, doc="Is this data type enabled for use?")

    UNICODE = UNICODE
    BOOLEAN = BOOLEAN
    INTEGER = INTEGER
    FLOAT = FLOAT
    DECIMAL = DECIMAL
    DATE = DATE
    DATETIME = DATETIME
    FILE = FILE
    SELECTION = SELECTION
    HTML = HTML
    JSON = JSON
    XML = XML

    system_type_description = "Data type descriptions for managed types."

    _value_class_cache = {}

    def get_serialize_keys(self):
        return super(ManagedFieldDataType, self).get_serialize_keys() + \
               ['description', 'capability', 'enabled', 'is_numeric']

    @staticmethod
    def add_default_records(system_type):
        """
        Boot some default data types.

        :param system_type: the parent record (the DataType system type.)
        :type system_type: SystemType
        :return: None
        """
        defaults = [
            ManagedFieldDataType.create(
                parent_id=system_type.id,
                name='Text', description='General text field.', capability=UNICODE),
            ManagedFieldDataType.create(
                parent_id=system_type.id,
                name='Boolean', description='Yes/no or on/off values.', capability=BOOLEAN),
            ManagedFieldDataType.create(
                parent_id=system_type.id,
                name='Integer', description='Integer (whole number) quantities.',
                capability=INTEGER),
            ManagedFieldDataType.create(
                parent_id=system_type.id,
                name='Float', description='Floating point (limited precision) quantities.',
                capability=FLOAT),
            ManagedFieldDataType.create(
                parent_id=system_type.id,
                name='Decimal', description='Decimal numbers and exact quantities.',
                # TODO: precision
                capability=DECIMAL),
            ManagedFieldDataType.create(
                parent_id=system_type.id,
                name='Date', description='Calendar date (without time).', capability=DATE),
            ManagedFieldDataType.create(
                parent_id=system_type.id,
                name='DateTime', description='Calendar date and time with timezone.',
                capability=DATETIME),
            ManagedFieldDataType.create(
                parent_id=system_type.id,
                name='File Attachment', description='File attachment with metadata.',
                capability=FILE),
            # ManagedFieldDataType(name='Blob', description='Binary object with no metadata.',
            # capability='blob'),
            ManagedFieldDataType.create(
                parent_id=system_type.id,
                name='Selection', description='A selection field linked to another domain type.',
                capability=SELECTION),
            ManagedFieldDataType.create(
                parent_id=system_type.id,
                name='HTML Data', description='Text in HTML format.',
                capability=HTML),
            ManagedFieldDataType.create(
                parent_id=system_type.id,
                name='XML Data', description='Data in XML format.',
                capability=XML),
            ManagedFieldDataType.create(
                parent_id=system_type.id,
                name='JSON Data', description='Data in JSON format.',
                capability=JSON),
        ]
        from .. import DBSession

        DBSession.add_all(defaults)

    @staticmethod
    def get_type_by_capability(capability):
        return DBSession.query(ManagedFieldDataType).filter_by(capability=capability).one()

    # noinspection PyUnusedLocal
    def is_numeric(self, request=None):
        return self.capability in NUMERIC_CAPABILITIES

    def get_value_class(self):
        # TODO: should be a class-level cache, not instance
        if not self._value_class_cache:
            self._value_class_cache = {
                UNICODE: ManagedUnicodeValue,
                BOOLEAN: ManagedBooleanValue,
                INTEGER: ManagedIntegerValue,
                FLOAT: ManagedFloatValue,
                DECIMAL: ManagedDecimalValue,
                DATE: ManagedDateValue,
                DATETIME: ManagedDateTimeValue,
                # INTERVAL: ManagedIntervalValue,
                # TIMEZONE: 'timezone',
                FILE: ManagedFileValue,
                # SELECTION: ManagedSelectionValue,
                # HTML: ManagedHTMLValue,
                # JSON: ManagedJSONValue,
                # XML: ManagedXMLValue,
            }
        return self._value_class_cache[self.capability]


class ManagedValueMixin(object):
    """
    Some utility methods common to ManagedValue types, mainly to represent them.
    """
    value = None
    object_field = None

    def __repr__(self):
        # return u"<%r :: %s>" % (self.field.name, self.value.__repr__(),)
        return self.value.__repr__()

    def __str__(self):
        return self.value.__str__()

    def format_value(self, request):
        value = self.value
        if self.object_field.field.data_type.is_numeric(request):
            value = request.localize_number(value)
        return value

    @staticmethod
    def get_serialize_keys():
        return ['obj_field_id', 'object_field', 'value']


class ManagedUnicodeValue(ManagedValueMixin, Base):
    __tablename__ = "managed_unicode"
    obj_field_id = Column(
        BigIntegerType,
        ForeignKey(
            'managed_object_field.id', onupdate=CASCADE, ondelete=CASCADE), primary_key=True)
    value = Column(
        UnicodeText, nullable=False, doc="Unicode string value for a managed object field.")

    object_field = relationship(
        "ManagedObjectField",
        backref=backref(UNICODE, cascade=rel_cascade, uselist=False, single_parent=True))

    @staticmethod
    def deserialize_attribute(attrib_name, attrib_value):
        """
        Convert user-supplied value to native type.
        """
        if attrib_name == "value":
            return u(attrib_value)
        return attrib_value


class ManagedBooleanValue(ManagedValueMixin, Base):
    __tablename__ = "managed_boolean"
    obj_field_id = Column(
        BigIntegerType,
        ForeignKey('managed_object_field.id', onupdate=CASCADE, ondelete=CASCADE), primary_key=True)
    value = Column(
        Boolean(name="value"), nullable=False,
        doc="Boolean value for a managed object field.")

    object_field = relationship(
        "ManagedObjectField",
        backref=backref(
            BOOLEAN, cascade=rel_cascade, uselist=False, single_parent=True))

    @staticmethod
    def deserialize_attribute(attrib_name, attrib_value):
        """
        Convert user-supplied value to native type.
        """
        if attrib_name == "value":
            return bool(attrib_value)
        return attrib_value


class ManagedIntegerValue(ManagedValueMixin, Base):
    __tablename__ = "managed_integer"
    obj_field_id = Column(
        BigIntegerType,
        ForeignKey('managed_object_field.id', onupdate=CASCADE, ondelete=CASCADE),
        primary_key=True)
    value = Column(
        BigIntegerType, nullable=False, doc="Integer value for a managed object field.")

    object_field = relationship(
        "ManagedObjectField",
        backref=backref(INTEGER, cascade=rel_cascade, uselist=False, single_parent=True))

    @staticmethod
    def deserialize_attribute(attrib_name, attrib_value):
        """
        Convert user-supplied value to native type.
        """
        if attrib_name == "value":
            # TODO: OK if not long in python 2?
            return int(attrib_value)
        return attrib_value


class ManagedFloatValue(ManagedValueMixin, Base):
    __tablename__ = "managed_float"
    obj_field_id = Column(
        BigIntegerType,
        ForeignKey('managed_object_field.id', onupdate=CASCADE, ondelete=CASCADE),
        primary_key=True)
    value = Column(Float(precision=53), nullable=False,
                   doc="Floating point value for a managed object field.")

    object_field = relationship(
        "ManagedObjectField",
        backref=backref(FLOAT, cascade=rel_cascade, uselist=False, single_parent=True))

    @staticmethod
    def deserialize_attribute(attrib_name, attrib_value):
        """
        Convert user-supplied value to native type.
        """
        if attrib_name == "value":
            return float(attrib_value)
        return attrib_value


class ManagedDecimalValue(ManagedValueMixin, Base):
    __tablename__ = "managed_decimal"
    obj_field_id = Column(
        BigIntegerType,
        ForeignKey('managed_object_field.id', onupdate=CASCADE, ondelete=CASCADE),
        primary_key=True)
    value = Column(Numeric(
        precision=DEC_PRECISION, scale=DEC_SCALE, asdecimal=True),
        nullable=False,
        doc="Decimal number value for a managed object field.")

    object_field = relationship(
        "ManagedObjectField",
        backref=backref(DECIMAL, cascade=rel_cascade, uselist=False, single_parent=True))

    @staticmethod
    def deserialize_attribute(attrib_name, attrib_value):
        """
        Convert user-supplied value to native type.
        """
        if attrib_name == "value":
            return Decimal(attrib_value)
        return attrib_value


class ManagedDateValue(ManagedValueMixin, Base):
    __tablename__ = "managed_date"
    obj_field_id = Column(
        BigIntegerType,
        ForeignKey('managed_object_field.id', onupdate=CASCADE, ondelete=CASCADE),
        primary_key=True)
    value = Column(
        Date(), nullable=False,
        doc="Date value for a managed object field.")

    object_field = relationship(
        "ManagedObjectField",
        backref=backref(DATE, cascade=rel_cascade, uselist=False,
                        single_parent=True))

    @staticmethod
    def deserialize_attribute(attrib_name, attrib_value):
        """
        Convert user-supplied value to native type.
        """
        if attrib_name == "value":
            new_date = dateutil.parser.parse(attrib_value)
            return new_date
        return attrib_value


class ManagedDateTimeValue(ManagedValueMixin, Base):
    __tablename__ = "managed_datetime"
    obj_field_id = Column(
        BigIntegerType,
        ForeignKey('managed_object_field.id', onupdate=CASCADE, ondelete=CASCADE),
        primary_key=True)
    value = Column(
        UTCDateTime(), nullable=False,
        doc="Datetime value in UTC zone for a managed object field.")

    object_field = relationship(
        "ManagedObjectField",
        backref=backref(DATETIME, cascade=rel_cascade, uselist=False, single_parent=True))

    @staticmethod
    def deserialize_attribute(attrib_name, attrib_value):
        """
        Convert user-supplied value to native type.
        """
        if attrib_name == "value":
            new_datetime = dateutil.parser.parse(attrib_value)
            return new_datetime
        return attrib_value


class ManagedFileValue(ManagedValueMixin, Base):
    """
    TODO: allow multiple attachments per field?
    """
    __tablename__ = "managed_file"
    obj_field_id = Column(
        BigIntegerType,
        ForeignKey(
            'managed_object_field.id', onupdate=CASCADE, ondelete=CASCADE), primary_key=True)
    value = Column(LargeBinary, nullable=False, doc="File attachment for a managed object field.")
    size = Column(BigIntegerType, nullable=False, doc="Size of the file in bytes.")
    mime_type = Column(Unicode(STD_NAME_LEN), nullable=False, doc="MIME type for the file.")
    file_name = Column(Unicode(STD_NAME_LEN), nullable=False, doc="Base filename.")
    extra_path = Column(UnicodeText, nullable=True,
                        doc="Original path or other extra path information.")
    description = Column(UnicodeText, nullable=True, doc="Extra description or comment field.")

    # modified_time
    # uploaded_by

    object_field = relationship(
        "ManagedObjectField",
        backref=backref(FILE, cascade=rel_cascade, uselist=False, single_parent=True))

    # TODO: implement deserialize_attribute!
