# !/usr/bin/env python
# -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-
# vim: fileencoding=utf-8 tabstop=4 expandtab shiftwidth=4

"""
Managed domain types are under runtime administrator control.
"""

import itertools
import sys
from logging import getLogger

from sqlalchemy import Column, ForeignKey, Boolean, Unicode, Integer, UnicodeText, UniqueConstraint
from sqlalchemy.event import listen
from sqlalchemy.orm import relationship, backref, reconstructor
from sqlalchemy.orm.exc import NoResultFound, DetachedInstanceError
from sqlalchemy.util import classproperty

from selmy.models import (
    BigIntegerType, rel_cascade, rel_delete_orphan, STD_NAME_LEN, CASCADE,
    NodeMixin, Base, UTCDateTime, SYSTEM, TYPE_ROOT, MANAGED_TYPE_ROOT,
    HybridNodeMixin, DBSession, before_update__modify_time)
# Helps avoid an mapping error with certain import paths in testing...
# Not sure if still necessary
from selmy.models.managed.managed_field_data_type import ManagedFieldDataType
from selmy.models.user import User
from selmy.util import u, repr_path, single_quote, utc_now

log = getLogger(__name__)

__author__ = 'Preston Landers (planders@gmail.com)'
__copyright__ = 'Copyright (c) 2017 Preston Landers'
__license__ = 'BSD'


class ManagedTypeDefinition(NodeMixin, Base):
    """
    Base type for managed types and fields - user defined resources.
    """
    __tablename__ = "managed_type_node"
    id = Column(BigIntegerType, primary_key=True)
    name = Column(Unicode(STD_NAME_LEN), nullable=False)
    parent_id = Column(BigIntegerType, ForeignKey(__tablename__ + '.id'))
    children = relationship(
        "ManagedTypeDefinition",
        # primaryjoin="managed_types.parent_id==managed_types.id",
        backref=backref('parent', remote_side=[id]),
        cascade=rel_delete_orphan, single_parent=True
    )
    type = Column(Unicode(STD_NAME_LEN), nullable=False)

    create_time = Column(UTCDateTime, doc="When this entry was created.", nullable=False)

    modify_time = Column(UTCDateTime, doc="When this entry was last modified.", nullable=False)

    description = Column(UnicodeText)

    locked = Column(Boolean(name='locked'), default=False, nullable=False,
                    doc="Is this type locked from modifications?")

    owner_id = Column(BigIntegerType, ForeignKey('user.id'), nullable=True)

    # doesn't do anything yet
    enabled = Column(Boolean(name='enabled'), default=True, nullable=False)

    owner = relationship(User, cascade=rel_cascade)

    # : Names must be unique under a given parent to ensure that resource lookup works.
    __table_args__ = (UniqueConstraint('parent_id', 'name', name="uq_managed_type_node_name"),)

    def __init__(self, *args, **kwargs):
        NodeMixin.__init__(self, *args, **kwargs)
        Base.__init__(self, *args, **kwargs)
        self._setup_data()

    # noinspection PyMethodParameters
    @classproperty
    def __mapper_args__(cls):
        return dict(
            polymorphic_on='type',
            polymorphic_identity=cls.__name__.lower(),
            with_polymorphic='*',
            # inherit_condition=join_condition(cls.__table__, cls.__table__, cls.id)
        )

    @reconstructor
    def _setup_data(self):
        """
        Enable this tree to locate itself within the master Pyramid resource tree.
        """
        self._resource_parent = None
        if not self.parent_id:
            root_type = RootType.get_root_instance()
            self._resource_parent = root_type[SYSTEM][TYPE_ROOT][MANAGED_TYPE_ROOT]

    def get_serialize_keys(self):
        return super(ManagedTypeDefinition, self).get_serialize_keys() + \
               ['locked', 'enabled', 'owner_id', 'owner']


class ManagedType(ManagedTypeDefinition):
    """
    Managed type definition (user-created domain type) resource.
    """
    __tablename__ = "managed_type"
    id = Column(BigIntegerType,
                ForeignKey('managed_type_node.id', onupdate=CASCADE, ondelete=CASCADE),
                primary_key=True)

    def get_child_class_for_name(self, content_name):
        # TODO: Is this always right? NO!!!
        return ManagedField

    def ensure_collections(self):
        from ..virtual_node import ManagedTypeRoot

        # Need to figure out something more clever than having
        # to remember to call this every time you create a type.
        # need to do same for SystemTypeDef
        my_type_parent = self.__parent__
        ## TODO: i18n errors
        if my_type_parent is None:
            raise ValueError(
                "You must give this object a parent before calling ensure_collections.")
        if not self.id:
            raise ValueError("This object must have an id before calling ensure_collections. "
                             "You may need to call DBSession.flush()")
        if isinstance(my_type_parent, ManagedTypeRoot):
            collection_parents = [
                RootType.get_root_instance()
            ]
        else:
            collection_parents = itertools.chain(*[
                parent_collection.children
                for parent_collection in my_type_parent.collections
            ])

        for db_obj in collection_parents:
            try:
                existing_collection = DBSession.query(ManagedCollection) \
                    .filter_by(
                    parent=db_obj,
                    managed_type=self,
                ).one()
            except NoResultFound:
                existing_collection = None
            if existing_collection:
                continue
            collection = ManagedCollection.create(
                name=self.name,
                parent=db_obj,
                parent_id=db_obj.id,
                managed_type_id=self.id,
            )
            log.debug(
                "Created %(root)s collection %(collection)s for type: %(type)s",
                dict(
                    root=db_obj.id,
                    collection=collection,
                    type=self,
                )
            )
            DBSession.add(collection)

        DBSession.flush()
        return

    def __getitem__(self, key):
        key = u(key)
        try:
            child_resource = DBSession.query(ManagedField).filter_by(
                name=key, parent_id=self.id).one()
        except NoResultFound:
            try:
                # To allow for true type hierarchies...
                child_resource = DBSession.query(ManagedType).filter_by(
                    name=key, parent_id=self.id).one()
            except NoResultFound:
                raise KeyError(key)
        return child_resource


class ManagedField(ManagedTypeDefinition):
    """
    This represents an individual field of a particular managed type.

    name is the name of the field.
    parent_id is the ManagedType
    """
    __tablename__ = "managed_field"
    id = Column(
        BigIntegerType, ForeignKey('managed_type_node.id', onupdate=CASCADE, ondelete=CASCADE),
        primary_key=True)
    data_type_id = Column(
        BigIntegerType,
        ForeignKey('managed_field_data_type.id', onupdate=CASCADE, ondelete=CASCADE),
        nullable=False)
    max_len = Column(
        Integer, default=STD_NAME_LEN, nullable=True,
        doc="Maximum string length on this field (optional)")
    order = Column(
        Integer, nullable=False, doc="Relative order within this managed type.")
    # selection_target_id = Column(BigIntegerType, ForeignKey('managed_types.id'),
    # nullable=True, doc="Optional target for Selection types.")

    # TODO: selection target field for built-in types?

    data_type = relationship('ManagedFieldDataType')

    # target_type = relationship('ManagedType',
    # # primaryjoin="managed_fields.selection_target_id==managed_types.id",
    # cascade=rel_cascade)

    @reconstructor
    def _setup_data(self):
        """
        Enable this tree to locate itself within the master Pyramid resource tree.
        """
        # The point here is to avoid calling parent classes.
        # We want to use the natural inheritance provided by NodeMixin
        return

    def get_serialize_keys(self):
        return super(ManagedField, self).get_serialize_keys() + \
               ['data_type_id', 'data_type', 'order', 'max_len']

    def get_serialize_summary_keys(self):
        return super(ManagedField, self).get_serialize_summary_keys() + \
               ['data_type', 'order', 'max_len']


class ManagedNode(NodeMixin, Base):
    """
    Base type for a resource which participates in the main managed types collection,
    so either a ManagedCollection or a ManagedObject (plus the special RootType.)
    """
    __tablename__ = "node"
    id = Column(BigIntegerType, primary_key=True)
    name = Column(Unicode(STD_NAME_LEN), nullable=False)
    parent_id = Column(BigIntegerType, ForeignKey(__tablename__ + '.id'))
    children = relationship(
        "ManagedNode",
        backref=backref('parent', remote_side=[id]),
        cascade=rel_delete_orphan, single_parent=True
    )
    type = Column(Unicode(STD_NAME_LEN), nullable=False)

    create_time = Column(UTCDateTime, doc="When this entry was created.", nullable=False)

    modify_time = Column(UTCDateTime, doc="When this entry was last modified.", nullable=False)

    # : Names must be unique under a given parent to ensure that resource lookup works.
    __table_args__ = (UniqueConstraint('parent_id', 'name', name="uq_node_name"),)

    def __init__(self, *args, **kwargs):
        NodeMixin.__init__(self, *args, **kwargs)
        Base.__init__(self, *args, **kwargs)
        self._setup_data()

    # noinspection PyMethodParameters
    @classproperty
    def __mapper_args__(cls):
        return dict(
            polymorphic_on='type',
            polymorphic_identity=cls.__name__.lower(),
            with_polymorphic='*',
        )

    @reconstructor
    def _setup_data(self):
        """
        Enable this tree to locate itself within the master Pyramid resource tree.
        """
        return


class ManagedCollection(ManagedNode):
    """
    name is same as the type_id -> managed_types.name
    parent_id is a ManagedObject
    """
    __tablename__ = "managed_collection"
    id = Column(
        BigIntegerType, ForeignKey('node.id', onupdate=CASCADE, ondelete=CASCADE),
        primary_key=True)
    managed_type_id = Column(
        BigIntegerType,
        ForeignKey('managed_type.id', onupdate=CASCADE, ondelete=CASCADE),
        nullable=False,
        doc="Which managed type is this?")
    managed_type = relationship(
        "ManagedType",
        backref=backref("collections", cascade=rel_delete_orphan, single_parent=True), )

    @reconstructor
    def _setup_data(self):
        """
        Enable this tree to locate itself within the master Pyramid resource tree.
        """
        self._resource_parent = None  # get from database
        if not self.parent_id:
            from ..managed import RootType

            root_type = RootType.get_root_instance()
            self._resource_parent = root_type

    def __getitem__(self, key):
        from .. import DBSession

        try:
            return DBSession.query(ManagedObject).filter_by(
                name=u(key), parent=self).one()
        except NoResultFound:
            raise KeyError(key)

    @property
    def description(self):
        return self.managed_type.description

    def get_serialize_keys(self):
        return super(ManagedCollection, self).get_serialize_keys() + \
               ['managed_type_id', 'managed_type', 'description', 'contents']

    def get_child_class_for_name(self, content_name):
        # Is this always right?
        return ManagedObject


class ManagedObject(ManagedNode):
    """
    Represents a managed object instance, an instantiation of the ManagedNode.

    name is the name of the object.
    parent_id is a ManagedCollection
    """
    __tablename__ = "managed_object"
    id = Column(
        BigIntegerType,
        ForeignKey(
            'node.id', onupdate=CASCADE, ondelete=CASCADE), primary_key=True)
    description = Column(UnicodeText)

    def __repr__(self):
        field_summary = []
        for field in self.children:
            if hasattr(field, "value"):
                disp_value = field.value
            else:
                disp_value = repr(field)
            field_summary.append("%s=%r" % (single_quote(field.name), disp_value))
        rpath = repr_path(self)
        return u"<Resource %s %s>" % (rpath, ",".join(field_summary))

    def __getitem__(self, key):
        key = u(key)
        try:
            result = DBSession.query(ManagedObjectField).filter_by(
                name=key, parent_id=self.id).one()
        except NoResultFound:
            try:
                # Is there a way to do this without repeated the query statement?
                # You can give m
                result = DBSession.query(ManagedCollection).filter_by(
                    name=key, parent_id=self.id).one()
            except NoResultFound:
                raise KeyError(key)
        return result

    def get_child_class_for_name(self, content_name):
        return ManagedObjectField

    # ensure values are returned in field order
    def values(self):
        sort_kids = []
        for child in self.children:
            child_field = getattr(child, "field", None)
            if child_field:
                sort_kids.append((child.field.order, child.field.order, child))
            else:
                # Move sub-type collections to the end of the list, sorted in their collection name.
                sort_kids.append((sys.maxsize, child.name, child))
        for rec in sorted(sort_kids):
            yield rec[2]

    def update_new_child_basic_values(self, new_child, new_name):
        """
        Update a ManagedObjectField to point to the correct field definition.
        """
        field = self.managed_type[new_name]
        field_id = field.id
        super(ManagedObject, self).update_new_child_basic_values(new_child, new_name)
        new_child.field_id = field_id
        new_child.field = field
        return new_child

    @property
    def managed_type(self):
        return self.parent.managed_type

    def ensure_collections(self):
        """
        We created a new instance of ManagedObject.
        We need to ensure collections exist for each subtype defined
        under our main type (self.managed_type)
        """
        # Need to figure out something more clever than having
        # to remember to call this every time you create a type.
        # need to do same for SystemTypeDef

        sub_types = [
            child for child in self.managed_type.children
            if isinstance(child, ManagedType)
        ]

        for sub_type in sub_types:
            collection = ManagedCollection.create(
                name=sub_type.name,
                parent=self,
                parent_id=self.id,
                managed_type_id=sub_type.id,
            )
            log.debug(
                "Created %(self)r collection %(collection)s",
                dict(
                    self=self,
                    collection=collection,
                )
            )
            DBSession.add(collection)

        DBSession.flush()
        return


class ManagedObjectField(ManagedNode):
    """
    Represents an individual field of an individual managed object.

    name is the same as field_id -> name
    parent_id is a ManagedObject
    """
    __tablename__ = "managed_object_field"
    id = Column(
        BigIntegerType,
        ForeignKey(
            'node.id', onupdate=CASCADE, ondelete=CASCADE), primary_key=True)
    field_id = Column(
        BigIntegerType,
        ForeignKey(
            'managed_field.id', onupdate=CASCADE, ondelete=CASCADE), nullable=False)

    field = relationship(
        'ManagedField',
        backref=backref("object_fields", cascade=rel_delete_orphan, single_parent=True),
        foreign_keys=[field_id])

    def managed_value(self, allow_create=True):
        """Return a reference to the correct ManagedValue"""
        # There has to be a better way to do this...
        cap = self.field.data_type.capability
        result = getattr(self, cap)
        if result is None and allow_create:
            result = self.field.data_type.get_value_class()()
            result.obj_field_id = self.field.id
            result.obj_field = self.field
            setattr(self, cap, result)
        return result

    @property
    def value(self):
        """Return the current value of this field (from the ManagedValue)."""
        return self.managed_value().value

    @value.setter
    def value(self, val):
        self.managed_value().value = val

    def __repr__(self):
        rpath = repr_path(self)
        return u"<Resource %s %r>" % (rpath, self.managed_value(allow_create=False))

    @property
    def description(self):
        return self.field.description

    def deserialize_attribute(self, attrib_name, attrib_value):
        if attrib_name in ("managed_value", "value"):
            return self.deserialize_value(attrib_value)
        return attrib_value

    def deserialize_value(self, attrib_value):
        return self.managed_value().deserialize_attribute("value", attrib_value)

    def format_value(self, request):
        return self.managed_value().format_value(request)

    def get_serialize_keys(self):
        return super(ManagedObjectField, self).get_serialize_keys() \
               + ['field', 'value', 'capability']

    def get_serialize_summary_keys(self):
        """
        Return the attributes to include in the summary JSON description.
        :return: list of attributes
        :rtype: list
        """
        return super(ManagedObjectField, self).get_serialize_summary_keys() \
               + ['value', 'capability']

    def update_from_json(self, patch_json):
        """
        Update value instances from a possibly incomplete JSON object,
        assumed to be user input from a POST/PATCH type command.

        TODO: VALIDATION IS NEEDED!
        """
        super(ManagedObjectField, self).update_from_json(patch_json)

        # Trigger a modification on the managed_object_field when
        # the dependent value has changed. Is there a better way to do this?
        # The value assigned below will actually be overwritten by the
        # event handler anyway
        if "value" in patch_json and hasattr(self, "modify_time"):
            self.modify_time = utc_now()  # Warning: this value is overwritten / disregarded!

        return self

    @property
    def capability(self):
        return self.field.data_type.capability


class RootType(HybridNodeMixin, ManagedNode):
    """
    RootType is a quasi-singleton. Each one of these forms the logical
    root of the entire domain type hierarchy for a system. Normally there is only
    ever one RootType record in the database (with an empty name) but future versions
    of this system may allow for multiple roots to be served by the same instance.

    RootType is not itself a DomainType because you, the user, cannot do anything
    with it. It doesn't visibly exist.

    RootType also maintains a few reserved resources, namely /System.
    """
    __tablename__ = "root"
    id = Column(
        BigIntegerType, ForeignKey(
            'node.id', onupdate=CASCADE, ondelete=CASCADE), primary_key=True)
    description = Column(UnicodeText)

    # : Entries which aren't looked up from the database (node table), currently /System
    __special_names__ = [SYSTEM]

    def __init__(self, *args, **kwargs):
        HybridNodeMixin.__init__(self, *args, **kwargs)
        ManagedNode.__init__(self, *args, **kwargs)
        self._setup_data()

    @classmethod
    def get_root_instance(cls, name=None):
        if name is None:
            name = u('')
        # TODO: ensure this is cached because it's called a lot.......
        return DBSession.query(cls).filter_by(name=name, parent_id=None).one()

    def get_path_name(self, unquote=False):
        if self.name:
            return self.name + u"/"
        return u""

    def __getitem__(self, key):
        key = u(key)
        try:
            return HybridNodeMixin.__getitem__(self, key)
        except KeyError:
            pass
        try:
            managed_collection = DBSession.query(ManagedCollection).filter_by(
                name=key, parent_id=self.id).one()
        except NoResultFound:
            raise KeyError(key)
        return managed_collection

    def __repr__(self):
        try:
            if self.name:
                return "<Resource /%s/>" % (self.name,)
        except (DetachedInstanceError, AttributeError):
            # TODO: temporary? for debugging toolbar
            pass

        return "<Resource />"

    @property
    def __parent__(self):
        return None

    @reconstructor
    def _setup_data(self):
        from selmy.models.virtual_node import SystemRoot, ManagedTypeRoot, TypeRoot

        super(RootType, self)._setup_data()
        system_root = SystemRoot(self)
        type_root = TypeRoot(system_root)
        system_root[type_root.name] = type_root
        managed_type_root = ManagedTypeRoot(type_root)
        type_root[managed_type_root.name] = managed_type_root
        self.data[system_root.name] = system_root


# Listen for update events
listen(ManagedType, 'before_update', before_update__modify_time, propagate=True)
listen(ManagedNode, 'before_update', before_update__modify_time, propagate=True)
