#!/usr/bin/env python
# -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-
# vim: fileencoding=utf-8 tabstop=4 expandtab shiftwidth=4

"""
Built-in system types.
"""

from importlib import import_module
from logging import getLogger

from sqlalchemy import Column, Unicode, UnicodeText, ForeignKey, UniqueConstraint
from sqlalchemy.orm import relationship, backref, reconstructor
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.util import classproperty
from sqlalchemy.event import listen
from selmy.models import (
    NodeMixin, Base, UTCDateTime, BigIntegerType, STD_NAME_LEN,
    rel_cascade, rel_delete_orphan, CASCADE, SYSTEM, TYPE_ROOT, before_update__modify_time
)
from selmy.util import get_full_class_name, u, single_quote, repr_path

__author__ = 'Preston Landers (planders@gmail.com)'
__copyright__ = 'Copyright (c) 2017 Preston Landers'
__license__ = 'BSD'

log = getLogger(__name__)


class SystemTypeDefinition(NodeMixin, Base):
    """
    Base class for all builtin System Type definitions.

    This constitutes its own logical tree in the database, but in the
    Pyramid resource hierarchy it falls under /System/Type
    """
    __tablename__ = "system_type_def"
    id = Column(BigIntegerType, primary_key=True)
    name = Column(Unicode(STD_NAME_LEN), nullable=False)
    parent_id = Column(BigIntegerType, ForeignKey(__tablename__ + '.id'))
    children = relationship(
        "SystemTypeDefinition",
        backref=backref('parent', remote_side=[id]),
        cascade=rel_delete_orphan, single_parent=True
    )
    type = Column(Unicode(STD_NAME_LEN), nullable=False)
    class_name = Column(Unicode(STD_NAME_LEN), nullable=False)

    create_time = Column(UTCDateTime, doc="When this entry was created.", nullable=False)

    modify_time = Column(UTCDateTime, doc="When this entry was last modified.", nullable=False)

    description = Column(UnicodeText)

    # : Names must be unique under a given parent to ensure that resource lookup works.
    __table_args__ = (UniqueConstraint('parent_id', 'name', name="uq_system_type_def_name"),)

    def __init__(self, *args, **kwargs):
        NodeMixin.__init__(self, *args, **kwargs)
        Base.__init__(self, *args, **kwargs)
        self._setup_data()

    # noinspection PyMethodParameters
    @classproperty
    def __mapper_args__(cls):
        return dict(
            polymorphic_on='type',
            polymorphic_identity=cls.__name__.lower(),
            with_polymorphic='*',
        )

    @staticmethod
    def add_system_default_records():
        from selmy.models.user import User, Role
        from selmy.models.managed.managed_field_data_type import ManagedFieldDataType

        from selmy.models.managed import RootType
        from selmy.models import DBSession

        # root_node = RootType(id=1, parent_id=None, name='', description="Root node")
        root_node = RootType.create(id=1, parent_id=None, name='', description="Root node")
        DBSession.add(root_node)
        DBSession.flush()

        system_types = [User, Role, ManagedFieldDataType]

        for systemClass in system_types:
            system_type_record = systemClass.create_system_type_def()
            DBSession.add(system_type_record)
            DBSession.flush()
            parent = None
            system_collection = systemClass.create_system_collection(
                system_type_record, parent_id=parent)
            DBSession.add(system_collection)
            DBSession.flush()
            systemClass.add_default_records(system_collection)

    # noinspection PyUnusedLocal
    @staticmethod
    def add_default_records(system_type_record):
        return

    @reconstructor
    def _setup_data(self):
        """
        Enable this tree to locate itself within the master Pyramid resource tree.
        """
        self._resource_parent = None
        if self.parent_id is None:
            from ..managed import RootType

            root_type = RootType.get_root_instance()
            self._resource_parent = root_type[SYSTEM][TYPE_ROOT]

    def get_class(self):
        """
        Get the actual class associated with a system type definition record.
        """
        # Is this the best way to do this??
        module_name, class_name = self.class_name.rsplit(".", 1)
        module = import_module(module_name)
        the_class = getattr(module, class_name, None)
        if the_class is None:
            raise KeyError(self.class_name)
        return the_class


class SystemTypeBase(NodeMixin, Base):
    """
    Base class for all builtin System Type instances, either
    SystemCollection or SystemType derived objects.

    Metaphorically those correspond to a folder and a file.

    This constitutes its own logical tree in the database, but in the
    Pyramid resource hierarchy it falls under /System
    """
    __tablename__ = "system"
    id = Column(BigIntegerType, primary_key=True)
    name = Column(Unicode(STD_NAME_LEN), nullable=False)
    parent_id = Column(BigIntegerType, ForeignKey(__tablename__ + '.id'))
    children = relationship(
        "SystemTypeBase",
        backref=backref('parent', remote_side=[id]),
        cascade=rel_delete_orphan, single_parent=True
    )
    type = Column(Unicode(STD_NAME_LEN), nullable=False)

    create_time = Column(UTCDateTime, doc="When this entry was created.", nullable=False)

    modify_time = Column(UTCDateTime, doc="When this entry was last modified.", nullable=False)

    # : Names must be unique under a given parent to ensure that resource lookup works.
    __table_args__ = (UniqueConstraint('parent_id', 'name', name="uq_system_name"),)

    # : Subclasses should override this to provide a description string for the type.
    system_type_description = "Generic system type."

    def __init__(self, *args, **kwargs):
        NodeMixin.__init__(self, *args, **kwargs)
        Base.__init__(self, *args, **kwargs)
        self._setup_data()

    # noinspection PyMethodParameters
    @classproperty
    def __mapper_args__(cls):
        return dict(
            polymorphic_on='type',
            polymorphic_identity=cls.__name__.lower(),
            with_polymorphic='*',
        )

    @property
    def system_type_name(self):
        return NotImplementedError("Subclasses must define the system_type_name property")

    @staticmethod
    def add_default_records(system_type_record):
        return

    @reconstructor
    def _setup_data(self):
        """
        Enable this tree to locate itself within the master Pyramid resource tree.
        """
        return


class SystemCollection(SystemTypeBase):
    """
    This places system objects in the resource hierarchy under a collection that
    belongs to some other object.
    """
    __tablename__ = "system_collection"
    id = Column(BigIntegerType, ForeignKey('system.id', onupdate=CASCADE, ondelete=CASCADE), primary_key=True)
    system_type_id = Column(BigIntegerType, ForeignKey('system_type_def.id', onupdate=CASCADE, ondelete=CASCADE),
                            nullable=False, doc="The ID of the system type definition.")
    system_type = relationship("SystemTypeDefinition", backref=backref("collections"), cascade=rel_cascade)

    # : Subclasses should override this to provide a description string for the type.
    system_type_description = "Collection of system objects."

    @reconstructor
    def _setup_data(self):
        """
        Enable this tree to locate itself within the master Pyramid resource tree.
        """
        self._resource_parent = None  # get from database
        if not self.parent_id:
            from ..managed import RootType

            root_type = RootType.get_root_instance()
            self._resource_parent = root_type[SYSTEM]

    def __getitem__(self, key):
        from .. import DBSession

        key = u(key)
        try:
            return DBSession.query(SystemType).filter_by(
                name=key, parent=self).one()
        except NoResultFound:
            raise KeyError(key)

    @property
    def description(self):
        return self.system_type.description

    def get_serialize_keys(self):
        return super(SystemCollection, self).get_serialize_keys() + \
            ['system_type_id', 'system_type', 'contents']


class SystemType(SystemTypeBase):
    """
    This places system objects in the resource hierarchy.
    """
    __tablename__ = "system_type"
    id = Column(BigIntegerType, ForeignKey('system.id', onupdate=CASCADE, ondelete=CASCADE), primary_key=True)

    def __init__(self, *args, **kwargs):
        super(SystemType, self).__init__(*args, **kwargs)

        if 'parent_id' not in kwargs:
            from selmy.models import DBSession
            # Get root system collection
            system_collection = DBSession.query(SystemCollection).filter_by(
                name=u(self.__class__.__name__),
                parent_id=None
            ).one()
            self.parent_id = system_collection.id

    @classmethod
    def create_system_type_def(cls):
        return SystemTypeDefinition.create(
            name=cls.system_type_name, parent_id=None,
            class_name=get_full_class_name(cls),
            description=cls.system_type_description)

    @classmethod
    def create_system_collection(cls, system_type, parent_id=None):
        return SystemCollection.create(
            name=system_type.name, parent_id=parent_id,
            system_type_id=system_type.id)

    def __repr__(self):
        field_summary = []
        # TODO: this code is duped in a couple places
        for field in self.children:
            field_summary.append("%s=%r" % (single_quote(field.name), field.value))
        rpath = repr_path(self)
        f_summary = " " + ",".join(field_summary) if field_summary else ""
        return u"<Resource %s %s>" % (rpath, f_summary)

# Listen for update events
listen(SystemTypeDefinition, 'before_update', before_update__modify_time, propagate=True)
listen(SystemTypeBase, 'before_update', before_update__modify_time, propagate=True)
