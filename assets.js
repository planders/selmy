const CSS = [
    ['bootstrap/dist', 'bootstrap'],
];
const JS = [
    ['bootstrap/dist', 'bootstrap'],
    ['jquery/dist', 'jquery'],
    ['jquery.cookie/jquery.cookie.js', 'jquery_cookie'],
    ['lodash/lodash.js', 'lodash/lodash.js'],
    ['lodash/lodash.min.js', 'lodash/lodash.min.js'],
    ['axios/dist', 'axios'],
    ['vue/dist', 'vue'],
    ['vue-router/dist', 'vue-router']
];

module.exports = [...JS, ...CSS];
