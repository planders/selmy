# Selmy Application Framework #

This is a Python application framework for supporting web and mobile apps.

It is designed to make it very easy to construct a basic business application,
and make it possible to do more interesting things.

Business application is loosely defined here to mean something that stores a database
of user-defined 'domain objects'; whatever is meaningful to the application. Anything
from a personal contacts app to a complex project management or time accounting system.

Selmy provides an extensible CRUD-style web interface to define and manage these business
objects and provides a RESTful API to manage them from web or mobile apps.

Selmy can be used as a standalone application for simple data management needs, or can be
used as a toolkit for creating more complex rules-driven business applications.

[ ![Codeship Status for planders/Selmy](https://www.codeship.io/projects/38a17640-ea58-0131-f280-6eb2be739762/status)](https://www.codeship.io/projects/26323)

# Planned Features #

Selmy aims to provide a number of useful features:

 * Quickly prototype and develop a useful multi-user database application that has a
   modern, customizable web front-end.
 * Security system with users, groups, and role-based authorization
    * Single sign-on and external authentication available but not required.
 * Pervasive reporting and data imports/exports in a variety of formats:
    * HTML
    * Plain Text (CSV)
    * External SQL reporting tools
    * Excel workbooks
 * All data is exposed and managed through a REST API.
    * Supporting mobile application development
    * As well as integrating with custom or 3rd party applications
 * An AngularJS based web front end.
 * Event-based notification and messaging system.
 * Workflow and business rules engine.
 * Provide high performance under a variety of database and server environments
   including Windows and Linux.
 * Full Internationalization / localization support
 * Scheduled Tasks
 * Background Tasks/Jobs
 * Auditing tied to notification system.
 * Basic CMS-like functions?

# Development #

## Dependencies ##

* Python 3.6
* Pyramid framework
* SQLAlchemy
* Related packages specified in setup.py

## Getting Started ##

- cd <directory containing this file>

- $VENV/bin/python setup.py develop

- cp development.ini local.ini

- (edit local.ini as desired)

- mkdir logs

- $VENV/bin/initialize_selmy_db local.ini

- $VENV/bin/pserve local.ini

## Useful Links ##

- [Pyramid Docs](http://docs.pylonsproject.org/projects/pyramid/en/latest/#)
- [Jinja2 Docs](http://jinja.pocoo.org/docs/)

## Internationalization ##

Following this process ensures that all parts of the application are translatable
to different languages.  Rather than hardcoding English text into the app,
create 'translation strings' as described below.  Then use the message catalog tools
to extract the translations strings into a .pot template file. Then update the message
catalog with the modified template, perform translation of the message catalog files
(the .po files), and then finally compile the catalog to binary format (the .mo files.)


### How to internationalize code ###

Create translation strings in app code rather than hardcoding English text. These can
be extracted and translated. Note that you cannot just directly display the translation
strings to the user because they won't be translated to the user's currently selected
language. For that you must use a localizer object to perform the translation.

* Jinja2 templates:
    * {{ gettext('The secret is: %(secret)s!', secret=secret) }}
      That creates a new extractable message. (Default text is same as ID.)
      Here, 'secret' is a variable obtained from the view.
      It also performs the localization (runtime substitution) directly when the template is evaluated.
    * Note that for translation strings which originate in Jinja2, we use a different
      'newstyle' format for parameters. It uses Python string formatting rules.
* Python code:
    * Import the translation string creator:
      from ..i18n import \_
    * Create a translation string, with optional default mapping:
        x = \_("The secret is: ${secret}", mapping={'secret': 42})
    * Get a translation manually, overriding the mapping if desired:
        x_trans = request.localizer.translate(x, mapping={'secret': '99'})
        If outside of a HTTP request, use selmy.bootstrap() to get an environment, then do
        env\['request'\].localizer.translate()
    * How to use a message ID different than the default value:
        x = \_('secret_answer', default="The secret is: ${secret}", mapping={'secret': 42})
    * Note that extractable translation strings created in Python code use different
      gettext-style ${} parameters compared to Jinja2.
    * You can add a translator comment above it:
      # TRANSLATORS: This frobnicates the widget
      foo = request.localizer.translate(_("Frobbing widget")
* Javascript: TODO

### Translate and Update Message Catalogs ###

To extract and translate message strings in the application, run this from the top level dir:

* Extract messages from application source:
    * python setup.py extract_messages
* Update the message catalog with the new/updated messages:
    * python setup.py update_catalog
* At this point, translate the messages in the selmy/locale/<NAME>/selmy.po files
* Now compile the messages to binary format and they're ready to run:
    * python setup.py compile_catalog

### Date and Time Handling ###

The database always stores datetimes in the UTC zone, however it does not use any DB native timezone 
datatype even if available. 
  
The Python code protects against TZ-naive datetimes going into the database.  The raw DB times
are automatically converted to TZ-aware datetimes coming out of the DB.

You can display a localized date/time by using request.localize_datetime(). If you do not provide
the locale, format, and display timezone, they will be provided for you.


## Selmy Philosophy and Design Principles ##

* Many if not most 'business apps' have a similar basic structure and requirements.
* Everything is a report
* Everything is extensible
* Everything is done through the REST API.
    * Hypertext as the engine of application state
* There is a data schema for domain objects but it can be modified at runtime
* Be a standard Python project as much as possible.
* Performance is important.
* The project started with essentially 100% test coverage - let's keep it that way.

# Design Information #

Selmy is built around several concepts:

* A "User" is anyone signed into the system or an anonymous guest user. A user
  can have 1 or more access roles (e.g. Manager) and group scopes.
* Domain Types are the core database table concept. A domain type represents
  something you want to store in the system: a project, a sales order, an invoice.
  Domain Types can be arranged in a hierarchy if so desired.
* Domain Objects are instances of a domain type: a specific sales order, for example.
* Domain Fields are data fields within the domain type. Domain types are comprised
  of a collection of Domain Fields.  Domain Fields can be one of several available types
  including references to other Domain Types.

## Core Model ##

General distinction between core types (below) and domain types.

* Users
* Roles
* Groups
* Domain Types registry
* Domain Values - core data storage
  * Each String type can have an alternate language version used according to the individual user's language pref
* Each domain type change regenerates its views to match?
* Events
  * Audit records of all changes

## Design Decisions ##

* Use Python instead of Go, Node.js, C#, or Rust
* Use Pyramid web framework, using both traversal and URL dispatch (a hybrid app in Pyramid terminology)
    * http://docs.pylonsproject.org/projects/pyramid/en/latest/designdefense.html
* Using Jinja2 for templating instead of Chameleon or Mako
* SQLAlchemy instead of ZDO (Zope object database) or MongoDB
    * Aim to support all database engines supported by SQLAlchemy.
* Use reStructuredText for docstrings, API docs - Sphinx to output

## Other Requirements ##

* Don't repeat yourself (DRY)
* All code should be Python 3 compliant - have a unit test to enforce
* Must work in a variety of WSGI server containers, IronPython, etc
    * IPv6 is supported to the extent the container supports it.
    * See also: https://github.com/Pylons/waitress/pull/66
* Internationalized (i18n) strings for both product text
    * And ideally also provide message catalog style options for user text. 
* Use absolute path imports.
* Adhere to standard Python naming and formatting conventions (PEP 8 stuff)
  * Turn on both PEP8 naming and style warnings in PyCharm
  * Module file names: lowercase_underscored  (corresponding class is CamelCase) 
  * SQL Table names: singular
    * Id fields: 'id'
    * Foreign keys: foo_id 
  * Collection objects: plural
* Pervasive auditing tied to notification/event system.
  * View object history


### Microsoft SQL Server ###

* Download and install the PyODBC driver appropriate to your version of Python, e.g.:
    pip install pyodbc

* In SQL Server, create an empty database to hold your site.
* Now configure the sqlalchemly.url in your local.ini file:
    http://docs.sqlalchemy.org/en/latest/dialects/mssql.html#module-sqlalchemy.dialects.mssql.pyodbc

* TODO: must patch apex\models.py to set naming convention:

```
    # : Establish a naming convention for constraints and other objects for consistency
    # : http://docs.sqlalchemy.org/en/rel_0_9/core/constraints.html#constraint-naming-conventions
    convention = {
        "ix": 'ix_%(column_0_label)s',
        "uq": "uq_%(table_name)s_%(column_0_name)s",
        "ck": "ck_%(table_name)s_%(constraint_name)s",
        "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
        "pk": "pk_%(table_name)s"
    }
    
    
    metadata = MetaData(naming_convention=convention)
    
    DBSession = scoped_session(sessionmaker(extension=ZopeTransactionExtension()))
    Base = declarative_base(metadata=metadata)
```    

* To use Windows Authentication from PyCharm's database console (which is a nifty feature, btw)
you must download and install: 

Microsoft JDBC Driver 4.0 for SQL Server
http://www.microsoft.com/en-us/download/details.aspx?displaylang=en&id=11774

This is basically just unzip a folder wherever you download it. From that folder, find:    

sqljdbc_auth.dll

(either x64 or x86 version as appropriate) and copy to the JRE used by PyCharm.

Normally this is in the PyCharm install directory. You should copy to the bin dir
 of that jre:

    <PyCharm Install Dir>\jre\jre\bin

For example:

    copy sqljdbc_auth.dll c:\Program Files\JetBrains\PyCharm 3.4\jre\jre\bin 

Now you can create a SQL Server database connection in PyCharm. In the connection
properties under Advanced, make sure integratedSecurity=true

You may need to restart PyCharm after copying the dll file.
(You can also copy the sqljdbc4.jar file to the jre/lib/ext directory to make it
use that version.)
    

# Open Source Credits and License #

TODO:

## Open Source Credits ##

This project incorporates code from:

* Twitter Bootstrap
* jQuery
* jQuery cookie plugin 
 * jQuery Cookie Plugin v1.4.1
 * https://github.com/carhartl/jquery-cookie
 * Copyright 2013 Klaus Hartl
 * Released under the MIT license
 