## General TODOs ##

After completing OpenID authentication you are only returned to the home route.
Would be nice if you went to the original URL. Should be possible with some kind
of authentication hook.

Try not to be so directly dependent on SQLAlchemy, although that might be a bit hard.
Or at least a lot of duplicated code...

## Security ##

We're currently using unencrypted session variables. Need to use either an encrypted session
or a server-side session like redis.

POSTing requires a CSRF token, but this has been temporarily disabled in the REST API.

http://docs.pylonsproject.org/projects/pyramid/en/latest/narr/sessions.html#preventing-cross-site-request-forgery-attacks

header: X-CSRF-Token

A temporary solution is to disable CSRF protection on all API requests but we need to 
do our own validation at the REST level. We need an API method to get the token.

Probably should also tie in to PUT and DELETE.

The config section to disable CSRF protection for the rest API is this:

apex.no_csrf = apex:apex_callback,restroot

## API ##

Allow recursive object creation. Currently this only works with managed fields, not child types.
 

## Database Stuff ##

Deleting 'Managed Fields' directly in SQL doesn't propagate to the Managed Object Fields.


## Development Environment ##

Script to setup virtualenv for development or deployment?

Need to separate out modules that are only needed for development and testing vs. the 
subset needed for actual deployment.
